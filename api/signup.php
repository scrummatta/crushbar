<?php 
    /*
    *
    * This is the part of the API that is responsible for register a user
    *
    */
    // Allow from any origin
    header('Access-Control-Allow-Origin: *');
    header("Content-type:multipart/form-data");
    header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-  Disposition, Content-Description');
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
    
    include_once 'conn.php'; //Include the connection with db.
    $conn = conn(); //Do the connection with server.

    //SET TIME ZONE FOR UTC
    date_default_timezone_set('America/Bahia');
    //Getting json code and converto to php data vector.
    $_POST = json_decode(file_get_contents('php://input'), true);

    //Check if the connection work.
    if ($conn->connect_error) {
        die('{"response":"'.$conn->connect_error.'"}');
        $conn->close();
    }else{
        //Getting email and pass of the new user
        $email = strtolower(htmlspecialchars($_POST['email']));
        $password = strtolower(htmlspecialchars($_POST['password']));
        //Request then on the user table
        $sql = "SELECT * FROM `cb_user` WHERE `email` = '$email'";
        $result = $conn->query($sql);

        if ($result && $result->num_rows <= 0) {//If then not exists on user table
            //Save then on the user table
            $sql = "INSERT INTO `cb_user` (`email`,`pass`,`is_deleted`) 
            VALUES ('$email','$password','false')";

            if ($conn->query($sql) === TRUE) {//If works..
                $new_id = $conn->insert_id;//Get the new inserted user id

                $version_db = htmlspecialchars($_POST['version_db']);
                $name = htmlspecialchars($_POST['name']);
                $lastname = htmlspecialchars($_POST['lastname']);
                $school = htmlspecialchars($_POST['school']);
                $job = htmlspecialchars($_POST['job']);
                $work = htmlspecialchars($_POST['work']);
                $bio = htmlspecialchars($_POST['bio']);
                $rangemin = htmlspecialchars($_POST['rangemin']);
                $rangemax = htmlspecialchars($_POST['rangemax']);
                $age = htmlspecialchars($_POST['age']);
                $gender = htmlspecialchars($_POST['gender']);
                $tel = htmlspecialchars($_POST['tel']);
                $choicewoman = htmlspecialchars($_POST['choicewoman']);
                $choiceman = htmlspecialchars($_POST['choiceman']);
                $showme = htmlspecialchars($_POST['showme']);
                $showmyage = htmlspecialchars($_POST['showmyage']);
                $imageprofile = htmlspecialchars($_POST['imageprofile']);
                $locationlat = htmlspecialchars($_POST['locationlat']);
                $locationlong = htmlspecialchars($_POST['locationlong']);

                //Insert this information on configuration table
                $sql = "INSERT INTO `cb_configuration` (`user_id`, `conf_key`, `key_value`) 
                VALUES ('$new_id','version_db','$version_db'),('$new_id','name','$name'), ('$new_id','lastname','$lastname'), 
                ('$new_id','school','$school'), ('$new_id','job','$job'), ('$new_id','work','$work'), ('$new_id','gender','$gender'), 
                ('$new_id','rangemin','$rangemin'), ('$new_id','rangemax','$rangemax'), ('$new_id','age','$age'), ('$new_id','bio','$bio'), 
                ('$new_id','location-long','$locationlong'), ('$new_id','location-lat','$locationlat'), 
                ('$new_id','imageprofile','$imageprofile'), ('$new_id','showmyage','$showmyage'), ('$new_id','showme','$showme'), 
                ('$new_id','choiceman','$choiceman'), ('$new_id','choicewoman','$choicewoman'), ('$new_id','tel','$tel')";
                if ($conn->query($sql) === TRUE) {//If works
                    echo '{"response": "success", "id": "'.$new_id.'"}';
                    $conn->close();
                } else {//If found any error
                    echo '{"response": "'.$conn->error.'"}';
                    $conn->close();
                }
            } else {//If not works..
                echo '{"response": "'.$conn->error.'"}';
                $conn->close();
            }
        } else if($result && $result->num_rows > 0){//If then exist..
            echo '{"response": "exist"}';
            $conn->close();
        }
    }
?>