<?php 
    /*
    *
    * This is the part of the API that is responsible for delete user
    *
    */
    // Allow from any origin
    header('Access-Control-Allow-Origin: *');
    header("Content-type:multipart/form-data");
    header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-  Disposition, Content-Description');
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");

    include_once 'conn.php'; //Include the connection with db.
    $conn = conn(); //Do the connection with server.
    //SET TIME ZONE FOR UTC
    date_default_timezone_set('America/Bahia');
    //Getting json code and converto to php data vector.
    $_POST = json_decode(file_get_contents('php://input'), true);

    //Check if the connection work.
    if ($conn->connect_error) {
        die('{"response":"'.$conn->connect_error.'"}');
        $conn->close();
    }else{
        $id = htmlspecialchars($_POST['id']);
        $value = "true";

        $sql = "UPDATE `cb_user` SET `is_deleted` = '$value' WHERE `id` = '$id'";
        if ($conn->query($sql) === TRUE) {
            echo '{"response": "success"}';
            $conn->close();
        } else {
            echo '{"response": "'.$conn->error.'"}';
            $conn->close();
        }
    }
?>