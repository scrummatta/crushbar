<?php 
    /*
    *
    * This is the part of the API that is responsible for get the user match feed
    *
    */
    // Allow from any origin
    header('Access-Control-Allow-Origin: *');
    header("Content-type:multipart/form-data");
    header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-  Disposition, Content-Description');
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
    
    include_once 'conn.php'; //Include the connection with db.
    $conn = conn(); //Do the connection with server.

    //SET TIME ZONE FOR UTC
    date_default_timezone_set('America/Bahia');
    //Getting json code and converto to php data vector.
    $_POST = json_decode(file_get_contents('php://input'), true);

    if ($conn->connect_error) {
        echo '{"response":"'.$conn->connect_error.'"}';
        $conn->close();
    }else{
        $action = htmlspecialchars($_POST['action']);
        $group_id = htmlspecialchars($_POST['group_id']);

        if($action == "set"){
            $message = htmlspecialchars($_POST['message']);
            $user_id = htmlspecialchars($_POST['id']);
            $date = date('d m Y H:i:s');

            $sql = "INSERT INTO `cb_message` (`group_id`, `user_id`, `message`, `date`, `is_deleted`) 
                    VALUES ('$group_id', '$user_id', '$message', '$date', 'false')";

            if ($conn->query($sql) === TRUE) {//If works
                $mes_id = $conn->insert_id;//Get the new inserted group id
                echo '{"response": "success", "mes_id":"'.$mes_id.'", "id":"'.$user_id.'", "date":"'.$date.'", "message":"'.$message.'"}';
            } else {//If found any error
                echo '{"response": "'.$conn->error.'"}';
            }
            $conn->close();
        }else if($action == "get"){
            $sql = "SELECT * FROM `cb_message` WHERE `group_id` = '$group_id' AND `is_deleted` = 'false' ORDER BY id LIMIT 20";
            $result = mysqli_query($conn, $sql);

            if ($result && $result->num_rows <= 0) {//Verify if exist
                echo '{"response": "empty"}';
                $conn->close();
            } else if($result && $result->num_rows > 0){//If exist...
                $messages = '{"messages":[';
                while($rowData = mysqli_fetch_array($result)){
                    $messages .= '{"mes_id":"'.$rowData["id"].'","id":"'.$rowData["user_id"].'", "message":"'.$rowData["message"].'", "date":"'.$rowData["date"].'"},';
                }
                if (strpos($messages, ',') !== false) {
                    $messages = substr_replace($messages ,'', -1);
                }
                echo $messages.'], "response":"success"}';
                $conn->close();
            }else {//If found any errors
                echo '{"response": "'.$conn->error.'"}';
                $conn->close();
            }
        }else if($action == "lasts"){
            $mesid = htmlspecialchars($_POST['mesid']);

            $sql = "SELECT * FROM `cb_message` WHERE `group_id` = '$group_id' AND `is_deleted` = 'false'";
            $result = mysqli_query($conn, $sql);

            if ($result && $result->num_rows <= 0) {//Verify if not exist
                echo '{"response": "empty"}';
                $conn->close();
            } else if($result && $result->num_rows > 0){//If exist...
                $messages = '{"messages":[';
                while($rowData = mysqli_fetch_array($result)){
                    if($rowData["id"] > $mesid){
                        $messages .= '{"mes_id":"'.$rowData["id"].'","id":"'.$rowData["user_id"].'", "message":"'.$rowData["message"].'", "date":"'.$rowData["date"].'",';

                        $uid = $rowData["user_id"];
                        $r = $conn->query("SELECT `key_value` FROM `cb_configuration` WHERE `user_id` = '$uid' AND `conf_key` = 'imageprofile'");
                        $r = mysqli_fetch_array($r);
                        $messages .= '"user_image":"'.$r["key_value"].'",';

                        $uid = $rowData["user_id"];
                        $r = $conn->query("SELECT `key_value` FROM `cb_configuration` WHERE `user_id` = '$uid' AND `conf_key` = 'name'");
                        $r = mysqli_fetch_array($r);
                        $messages .= '"user_name":"'.$r["key_value"].'"},';
                    }
                }
                if (strpos($messages, ',') !== false) {
                    $messages = substr_replace($messages ,'', -1);
                }
                echo $messages.'], "response":"success"}';
                $conn->close();
            }else {//If found any errors
                echo '{"response": "'.$conn->error.'"}';
                $conn->close();
            }
        }else{
            echo '{"response":"action_not_found"}';
            $conn->close();
        }
    }
?>