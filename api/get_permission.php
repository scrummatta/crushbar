<?php 
    /*
    *
    * This is the part of the API that is responsible for get the user match feed
    *
    */
    // Allow from any origin
    header('Access-Control-Allow-Origin: *');
    header("Content-type:multipart/form-data");
    header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-  Disposition, Content-Description');
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
    
    include_once 'conn.php'; //Include the connection with db.
    $conn = conn(); //Do the connection with server.

    //SET TIME ZONE FOR UTC
    date_default_timezone_set('America/Bahia');
    //Getting json code and converto to php data vector.
    $_POST = json_decode(file_get_contents('php://input'), true);

    if ($conn->connect_error) {
        die('{"response":"'.$conn->connect_error.'"}');
        $conn->close();
    }else{
        $action = htmlspecialchars($_POST['action']);
        if($action == 'hour'){
            $daywork = date('D H:i:s');
            $isOpen = false;
            $sql = "SELECT * FROM `pan_work`";
            $result = $conn->query($sql);

            if ($result && $result->num_rows <= 0) {//Verify if exist
                $isOpen = false;
            } else if($result && $result->num_rows > 0){//If exist...
                while($rowData = mysqli_fetch_array($result)){
                    if(strtotime($rowData["open"]) >= strtotime($daywork) && strtotime($rowData["close"]) <= strtotime($daywork)){
                        $isOpen = true;
                        break;
                    }
                }
            }
            if($isOpen){
                echo '{"response":"opened"}';
            }else{
                echo '{"response":"closed"}';
            }
            $conn->close();
        }else{
            echo '{"response":"action_not_found"}';
            $conn->close();
        }
    }
?>