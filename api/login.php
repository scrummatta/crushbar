<?php 
    /*
    *
    * This is the part of the API that is responsible for logging in
    *
    */
    // Allow from any origin
    header('Access-Control-Allow-Origin: *');
    header("Content-type:multipart/form-data");
    header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-  Disposition, Content-Description');
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");

    include_once 'conn.php'; //Include the connection with db.
    $conn = conn(); //Do the connection with server.

    //SET TIME ZONE FOR UTC
    date_default_timezone_set('America/Bahia');
    //Getting json code and converto to php data vector.
    $_POST = json_decode(file_get_contents('php://input'), true);

    //Check if the connection work.
    if ($conn->connect_error) {
        die('{"response":"'.$conn->connect_error.'"}');
        $conn->close();
    }else{
        //Get email and pass of the user
        $email = strtolower(htmlspecialchars($_POST['email']));
        $password = strtolower(htmlspecialchars($_POST['password']));
        //Create the sql request
        $sql = "SELECT `id` FROM `cb_user` WHERE `email` = '$email' AND `pass` = '$password' AND `is_deleted` = 'false'";
        $result = mysqli_query($conn, $sql);

        if ($result && $result->num_rows <= 0) {//Cheking if user exist
            echo '{"response": "not_found"}';
            $result->close();
        } else if($result && $result->num_rows > 0){//And if exist, send his id
            $result = mysqli_fetch_array($result);
            echo '{"response": "success", "id": "'.$result["id"].'"}';
        }else {//Do this if found any error
            echo '{"response": "'.$conn->error.'"}';
            $result->close();
        }
    }
?>