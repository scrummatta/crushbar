<?php 
    /*
    *
    * This is the part of the API that is responsible for get the user match feed
    *
    */
    // Allow from any origin
    header('Access-Control-Allow-Origin: *');
    header("Content-type:multipart/form-data");
    header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-  Disposition, Content-Description');
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
    
    include_once 'conn.php'; //Include the connection with db.
    $conn = conn(); //Do the connection with server.

    //SET TIME ZONE FOR UTC
    date_default_timezone_set('America/Bahia');
    //Getting json code and converto to php data vector.
    $_POST = json_decode(file_get_contents('php://input'), true);

    if ($conn->connect_error) {
        die('{"response":"'.$conn->connect_error.'"}');
        $conn->close();
    }else{
        $action = htmlspecialchars($_POST['action']);

        if($action == "set"){
            $id = htmlspecialchars($_POST['id']);
            $user_two = htmlspecialchars($_POST['user_two']);

            $sql = "SELECT * FROM `cb_groups` WHERE `user_one` = '$id' AND `user_two` = '$user_two'";
            $result = $conn->query($sql);
            if ($result->num_rows <= 0) {
                $sql = "SELECT * FROM `cb_groups` WHERE `user_one` = '$user_two' AND `user_two` = '$id'";
                $result = $conn->query($sql);
                
                if ($result->num_rows <= 0) {
                    $sql = "INSERT INTO `cb_groups` (`user_one`, `user_two`, `is_deleted`) VALUES ('$id', '$user_two', 'false')";

                    if ($conn->query($sql) === TRUE) {//If works
                        $group_id = $conn->insert_id;//Get the new inserted group i
                        $message = htmlspecialchars($_POST['message']);
                        $date = date('d m Y H:i:s');

                        $sql = "INSERT INTO `cb_message` (`group_id`, `user_id`, `message`, `date`, `is_deleted`) 
                                VALUES ('$group_id', '$id', '$message', '$date', 'false')";

                        if ($conn->query($sql) === TRUE) {//If works
                            $mes_id = $conn->insert_id;//Get the new inserted group id
                            echo '{"response": "success", "group_id":"'.$group_id.'",';
                            echo '"mes_id":"'.$mes_id.'", "id":"'.$id.'", "date":"'.$date.'", "message":"'.$message.'"}';
                        } else {//If found any error
                            echo '{"response": "'.$conn->error.'"}';
                        }
                        $sql = "UPDATE `cb_match` SET `is_deleted` = 'true' WHERE `user_id` = '$id' AND `matched_id` = '$user_two'";
                        $conn->query($sql);
                        $sql = "UPDATE `cb_match` SET `is_deleted` = 'true' WHERE `user_id` = '$user_two' AND `matched_id` = '$id'";
                        $conn->query($sql);
                    } else {//If found any error
                        echo '{"response": "'.$conn->error.'"}';
                    }
                    $conn->close();
                } else{
                    $result = mysqli_fetch_array($result);
                    $group_id = $result['id'];
                    echo '{"response": "exist", "group_id": "'.$group_id.'"}';
                }
            } else{
                $result = mysqli_fetch_array($result);
                $group_id = $result['id'];
                echo '{"response": "exist", "group_id": "'.$group_id.'"}';
            }
        }else if($action == "get"){
            $id = htmlspecialchars($_POST['id']);
            $sql = "SELECT * FROM `cb_groups` WHERE `user_one` = '$id' OR `user_two` = '$id'";
            $result = $conn->query($sql);

            if ($result && $result->num_rows <= 0) {//Verify if exist
                echo '{"response": "empty"}';
                $conn->close();
            } else if($result && $result->num_rows > 0){//If exist...
                $group = '{"groups":[';
                while($rowData = mysqli_fetch_array($result)){
                    if($rowData["is_deleted"] == 'false'){
                        $group_id = $rowData["id"];
                        if($rowData["user_one"] == $id){
                            $uid = $rowData["user_two"];

                            $r = $conn->query("SELECT `key_value` FROM `cb_configuration` WHERE `user_id` = '$uid' AND `conf_key` = 'imageprofile'");
                            $r = mysqli_fetch_array($r);
                            $group .= '{"id":"'.$group_id.'","user_two":"'.$uid.'", "user_image":"'.$r["key_value"].'",';

                            $r = $conn->query("SELECT `key_value` FROM `cb_configuration` WHERE `user_id` = '$uid' AND `conf_key` = 'name'");
                            $r = mysqli_fetch_array($r);
                            $group .= '"user_name":"'.$r["key_value"].'",';
                            
                            $r = $conn->query("SELECT * FROM `cb_message` WHERE `group_id` = '$group_id' AND `is_deleted` = 'false' ORDER BY id DESC");
                            $r = mysqli_fetch_array($r);
                            $group .= '"message":"'.$r["message"].'"},';
                        }else{
                            $uid = $rowData["user_one"];

                            $r = $conn->query("SELECT `key_value` FROM `cb_configuration` WHERE `user_id` = '$uid' AND `conf_key` = 'imageprofile'");
                            $r = mysqli_fetch_array($r);
                            $group .= '{"id":"'.$group_id.'","user_two":"'.$uid.'", "user_image":"'.$r["key_value"].'",';

                            $r = $conn->query("SELECT `key_value` FROM `cb_configuration` WHERE `user_id` = '$uid' AND `conf_key` = 'name'");
                            $r = mysqli_fetch_array($r);
                            $group .= '"user_name":"'.$r["key_value"].'",';
                            
                            $r = $conn->query("SELECT * FROM `cb_message` WHERE `group_id` = '$group_id' AND `is_deleted` = 'false' ORDER BY id DESC");
                            $r = mysqli_fetch_array($r);
                            $group .= '"message":"'.$r["message"].'"},';
                        }
                    }
                }
                if (strpos($group, ',') !== false) {
                    $group = substr_replace($group ,'', -1);
                }
                echo $group.'], "response":"success"}';
                $conn->close();
            }else {//If found any errors
                echo '{"response": "'.$conn->error.'"}';
                $conn->close();
            }
        }else if ($action == "getthis"){
            $group_id = htmlspecialchars($_POST['group_id']);
            $sql = "SELECT * FROM `cb_groups` WHERE `id` = '$group_id'";
            $result = $conn->query($sql);

            if ($result && $result->num_rows <= 0) {//Verify if exist
                echo '{"response": "empty"}';
            } else if($result && $result->num_rows > 0){//If exist...
                $result = mysqli_fetch_array($result);
                $user_one = $result["user_one"];
                $user_two = $result["user_two"];
                echo '{"response": "success", "user_one": "'.$user_one.'", "user_two": "'.$user_two.'"}';
            }
            $conn->close();
        }else if($action == "del"){
            $group_id = htmlspecialchars($_POST['group_id']);
            //Updating on table
            $sql = "UPDATE `cb_groups` SET `is_deleted` = 'true' WHERE `id` = '$group_id'";

            if ($conn->query($sql) === TRUE) {//If works..
                echo '{"response": "success"}';
            } else {//And if found any errors..
                echo '{"response": "'.$conn->error.'"}';
            }
            $conn->close();
        }else{
            echo '{"response":"action_not_found"}';
            $conn->close();
        }
    }
?>