<?php 
    /*
    *
    * This is the part of the API that is responsible for verify if e-mail is registered or not
    *
    *
    * Configuration Key:
    *    version_db: int
    *    --
    *    name: string | size 20
    *    lastname: string | size 20
    *    bio: string | size 500
    *    school: string | size 50
    *    job: strin | 50
    *    work: strin | 50
    *    --
    *    rangemin: int
    *    rangemax: int
    *    age: int
    *    gender: 0/1
    *    tel: string | size 11
    *    --
    *    choicewoman : 0/1
    *    choiceman : 0/
    *    --
    *    showme: 0/1
    *    showmyage: 0/1
    *    --
    *    imageprofile: image_url
    *    image: url
    *    --
    *    location-lat: float
    *    location-long: float
    *    --
    *    matched: user_id_matched
    *    like: user_liked_id
    */
    // Allow from any origin
    header('Access-Control-Allow-Origin: *');
    header("Content-type:multipart/form-data");
    header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-  Disposition, Content-Description');
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
    
    include_once 'conn.php'; //Include the connection with db.
    $conn = conn(); //Do the connection with server.

    //SET TIME ZONE FOR UTC
    date_default_timezone_set('America/Bahia');
    //Getting json code and converto to php data vector.
    $_POST = json_decode(file_get_contents('php://input'), true);

    //Check if the connection work.
    if ($conn->connect_error) {
        die('{"response":"'.$conn->connect_error.'"}');
        $conn->close();
    }else{
        //Getting action and user id
        $action = htmlspecialchars($_POST['action']);
        $id = htmlspecialchars($_POST['id']);
        //Checking the action
        if($action == "get"){//This action return all user configuration for informed id.
            //Do the request for the server
            $sql = "SELECT * FROM `cb_configuration` WHERE `user_id` = '$id'";
            $result = $conn->query($sql);

            if ($result && $result->num_rows <= 0) {//Verify if key exist
                echo '{"response": "key_not_found"}';
                $conn->close();
            } else if($result && $result->num_rows > 0){//If exist...
                echo '{';
                while($rowData = mysqli_fetch_array($result)) {//Make the json object of keys
                    $string = $rowData["key_value"];
                    echo '"'.$rowData["conf_key"].'": "'.preg_replace( "/\r|\n/", "", $string ).'",';
                }
                echo '"response":"success"}';
                $conn->close();
            }else {//If found any errors
                echo '{"response": "'.$conn->error.'"}';
                $conn->close();
            }
        }else if($action == "upd"){//Update a value of any key on configuration table
            //Getting the key name and his value
            $key = htmlspecialchars($_POST['key']);
            $value = htmlspecialchars($_POST['value']);
            //Updating on table
            $sql = "UPDATE `cb_configuration` SET `key_value` = '$value' WHERE `user_id` = '$id' AND `conf_key` = '$key'";

            if ($conn->query($sql) === TRUE) {//If works..
                echo '{"response": "success"}';
                $conn->close();
            } else {//And if found any errors..
                echo '{"response": "'.$conn->error.'"}';
                $conn->close();
            }
        }else{//If informed action is invalid
            echo '{"response":"action_not_found"}';
            $conn->close();
        } 
    }
?>