<?php 
    /*
    *
    * This is the part of the API that is responsible for verify if e-mail is registered or not
    *
    */
    // Allow from any origin
    header('Access-Control-Allow-Origin: *');
    header("Content-type:multipart/form-data");
    header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-  Disposition, Content-Description');
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");

    include_once 'conn.php'; //Include the connection with db.
    $conn = conn(); //Do the connection with server.

    //SET TIME ZONE FOR UTC
    date_default_timezone_set('America/Bahia');
    //Getting json code and converto to php data vector.
    $_POST = json_decode(file_get_contents('php://input'), true);

    //Check if the connection work.
    if ($conn->connect_error) {
        die('{"response":"'.$conn->connect_error.'"}');
        $conn->close();
    }else{
        //Receive the signup email
        $email = strtolower(htmlspecialchars($_POST['email']));
        //Request then on the user table
        $sql = "SELECT * FROM `cb_user` WHERE `email` = '$email'";
        $result = mysqli_query($conn, $sql);

        if ($result && $result->num_rows <= 0) {//If then not exists on user table
            echo '{"response": "success"}';
            $result->close();
        } else if($result && $result->num_rows > 0){//If then exist..
            echo '{"response": "exist"}';
            $result->close();
        }else {//If found any error..
            echo '{"response": "'.$conn->error.'"}';
            $result->close();
        }
    }
?>