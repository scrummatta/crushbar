<?php 
    /*
    *
    * This is the part of the API that is responsible for upload, get and delete an image in the user profile
    *
    */
    // Allow from any origin
    header('Access-Control-Allow-Origin: *');
    header("Content-type:image/jpg");
    header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");

    include_once 'conn.php'; //Include the connection with db.
    $conn = conn(); //Do the connection with server.

    //SET TIME ZONE FOR UTC
    date_default_timezone_set('America/Bahia');

    //Check if the connection work.
    if ($conn->connect_error) {
        die('{"response":"'.$conn->connect_error.'"}');
        $conn->close();
    }else{
        //Getting action and user id
        $action = htmlspecialchars($_POST['action']);
        $id = htmlspecialchars($_POST['id']);
        //Make target dir
        $target_dir = 'user_images/'.$id.'/images/';
        //Use this to check if upload gone
        $uploadOk = 1;
        if($action == "set"){
            //Check if path exist, and if not execute the mkdir
            if(!file_exists($target_dir)){
                mkdir($target_dir, 0777, true);
            }
            //Make the target_file complete URI
            $target_file = $target_dir . basename($_FILES['photo']['name']);
            //Getting the type of the uploaded image
            $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
            if ($_FILES["photo"]["size"] > 500000) {//Checking image size
                echo '{"response":"size_error", "size":"'.$_FILES["photo"]["size"].'"}';
                $uploadOk = 0;
            }else{
                $check = getimagesize($_FILES["photo"]["tmp_name"]);
                if($check === false) {//Checking if file realy exist
                    echo '{"response":"file_error"}';
                    $uploadOk = 0;
                } else if ($uploadOk == 1) {//If everything is Okay, uploadOk is equal 1...
                    if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {//and the image is saved on server path
                        //Creating URL and deleted information for Db control
                        $url = "https://www.bardapaquera.com.br/api/".$target_file;
                        $is_deleted = "false";
                        //Put this url on DB to get after
                        $sql = "INSERT INTO `cb_images` (`user_id`,`url`,`is_deleted`) VALUES ('$id','$url','$is_deleted')";
                        if ($conn->query($sql) === TRUE) {//If works
                            $img_id = $conn->insert_id;//Get the new inserted user id
                            $conn->close();
                            //Send response
                            echo '{"response":"success","url":"'.$url.'","id":"'.$img_id.'"}';
                        } else {//If found any error
                            echo '{"response": "'.$conn->error.'"}';
                            $conn->close();
                        }
                    } else {//But if upload get any error...
                        echo '{"response":"not_success"}';
                    }
                }
            }
        }else if($action == "profset"){
            //Check if path exist, and if not execute the mkdir
            if(!file_exists($target_dir)){
                mkdir($target_dir, 0777, true);
            }
            //Make the target_file complete URI
            $target_file = $target_dir . basename($_FILES['photo']['name']);
            //Getting the type of the uploaded image
            $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
            if ($_FILES["photo"]["size"] > 500000) {//Checking image size
                echo '{"response":"size_error", "size":"'.$_FILES["photo"]["size"].'"}';
                $uploadOk = 0;
            }else{
                $check = getimagesize($_FILES["photo"]["tmp_name"]);
                if($check === false) {//Checking if file realy exist
                    echo '{"response":"file_error"}';
                    $uploadOk = 0;
                } else if ($uploadOk == 1) {//If everything is Okay, uploadOk is equal 1...
                    if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {//and the image is saved on server path
                        //Creating URL and deleted information for Db control
                        $url = "https://www.bardapaquera.com.br/api/".$target_file;
                        $key = "imageprofile";
                        //Put this url on DB to get after
                        $sql = "UPDATE `cb_configuration` SET `key_value` = '$url' WHERE `user_id` = '$id' AND `conf_key` = '$key'";
                        if ($conn->query($sql) === TRUE) {//If works
                            //Send response
                            echo '{"response":"success","url":"'.$url.'"}';
                            $conn->close();
                        } else {//If found any error
                            echo '{"response": "'.$conn->error.'"}';
                            $conn->close();
                        }
                    } else {//But if upload get any error...
                        echo '{"response":"not_success"}';
                    }
                }
            }
        }else{//If anyone action is not found
            echo '{"response":"action_not_found"}';
        }
    }
?>