<?php 
    /*
    *
    * This is the part of the API that is responsible for get the user match feed
    *
    */
    // Allow from any origin
    header('Access-Control-Allow-Origin: *');
    header("Content-type:multipart/form-data");
    header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-  Disposition, Content-Description');
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
    
    include_once 'conn.php'; //Include the connection with db.
    $conn = conn(); //Do the connection with server.

    //SET TIME ZONE FOR UTC
    date_default_timezone_set('America/Bahia');
    //Getting json code and converto to php data vector.
    $_POST = json_decode(file_get_contents('php://input'), true);

    if ($conn->connect_error) {
        die('{"response":"'.$conn->connect_error.'"}');
        $conn->close();
    }else{
        $action = htmlspecialchars($_POST['action']);
        $id = htmlspecialchars($_POST['id']);

        if($action == "set"){
            $like_id = htmlspecialchars($_POST['like_id']);
            $like_type = htmlspecialchars($_POST['like_type']);

            $sql = "SELECT * FROM `cb_like` WHERE `user_id` = '$like_id' AND `user_liked` = '$id'";
            $r = $conn->query($sql);
            
            if($like_type == 'like'){
                if ($r && $r->num_rows <= 0) {
                    $sql = "INSERT INTO `cb_like` (`user_id`, `user_liked`, `like_type`) VALUES ('$id', '$like_id', '$like_type')";
                    if ($conn->query($sql) === TRUE) {//If works
                        echo '{"response": "success"}';
                    } else {//If found any error
                        echo '{"response": "'.$conn->error.'"}';
                    }
                    $conn->close();
                } else if($r && $r->num_rows > 0){
                    $r = mysqli_fetch_array($r);
                    $type = $r["like_type"];
                    $st = '{"response": "liked", "like_type": "'.$type.'"}';
                    
                    $sql = "DELETE FROM `cb_like` WHERE `user_id` = '$like_id' AND `user_liked` = '$id'";
                    if ($conn->query($sql) === TRUE) {//If works
                        $sql = "INSERT INTO `cb_match` (`user_id`, `matched_id`, `match_type`, `notify`, `is_deleted`) VALUES ('$like_id', '$id', '$like_type', 'false', 'false')";
                        if ($conn->query($sql) === TRUE) {//If works
                            $sql = "INSERT INTO `cb_match` (`user_id`, `matched_id`, `match_type`, `notify`, `is_deleted`) VALUES ('$id', '$like_id', '$type', 'false', 'false')";
                            if ($conn->query($sql) === TRUE) {//If works
                                echo $st;
                            } else {//If found any error
                                echo '{"response": "'.$conn->error.'"}';
                            }
                        } else {//If found any error
                            echo '{"response": "'.$conn->error.'"}';
                        }
                        $conn->close();
                    } else {//If found any error
                        echo '{"response": "'.$conn->error.'"}';
                        $conn->close();
                    }
                }else{
                    echo '{"response": "'.$conn->error.'"}';
                    $conn->close();
                }
            }else if($like_type == 'superlike'){
                if ($r && $r->num_rows > 0) {
                    $r = mysqli_fetch_array($r);
                    $sql = "DELETE FROM `cb_like` WHERE `user_id` = '$like_id' AND `user_liked` = '$id'";
                    $conn->query($sql);
                }
                $sql = "INSERT INTO `cb_match` (`user_id`, `matched_id`, `match_type`, `notify`, `is_deleted`) VALUES ('$like_id', '$id', '$like_type', 'false', 'false')";
                if ($conn->query($sql) === TRUE) {//If works
                    $sql = "INSERT INTO `cb_match` (`user_id`, `matched_id`, `match_type`, `notify`, `is_deleted`) VALUES ('$id', '$like_id', '$like_type', 'true', 'false')";
                    if ($conn->query($sql) === TRUE) {//If works
                        echo '{"response": "liked"}';
                    } else {//If found any error
                        echo '{"response": "'.$conn->error.'"}';
                    }
                } else {//If found any error
                    echo '{"response": "'.$conn->error.'"}';
                }
                $conn->close();
            }
        }else if($action == "get"){
            $sql = "SELECT * FROM `cb_match` WHERE `user_id` = '$id' AND `is_deleted` = 'false' ORDER BY id DESC";
            $result = mysqli_query($conn, $sql);

            if ($result && $result->num_rows <= 0) {//Verify if exist
                echo '{"response": "not_found"}';
                $conn->close();
            } else if($result && $result->num_rows > 0){//If exist...
                $match = '{"match":[';
                while($rowData = mysqli_fetch_array($result)) {//Make the json object
                    $matched_id = $rowData["matched_id"];
                    $notify = $rowData["notify"];
                    $type = $rowData["match_type"];
                    //Do the request for the server
                    $sql = "SELECT * FROM `cb_configuration` WHERE `user_id` = '$matched_id'";
                    $r = mysqli_query($conn, $sql);
                    if($r && $r->num_rows > 0){//If exist...
                        $match .= '{"matched_id": "'.$matched_id.'",';
                        while($rData = mysqli_fetch_array($r)) {//Make the json object of keys
                            $match .= '"'.$rData["conf_key"].'": "'.$rData["key_value"].'",';
                        }
                        $match .= '"notify": "'.$notify.'", "type": "'.$type.'"},';
                    }
                }
                if (strpos($match, ',') !== false) {
                    $match = substr_replace($match ,'', -1);
                }
                echo $match.'],"response":"success"}';
                $conn->close();
            }else {//If found any errors
                echo '{"response": "'.$conn->error.'"}';
                $conn->close();
            }
        }else if($action == "upd"){
            $matched_id = htmlspecialchars($_POST['matched_id']);
            $sql = "UPDATE `cb_match` SET `notify` = 'true' WHERE `user_id` = '$id' AND `matched_id` = '$matched_id'";

            if ($conn->query($sql) === TRUE) {//If works..
                echo '{"response": "success"}';
            } else {//And if found any errors..
                echo '{"response": "'.$conn->error.'"}';
            }
            $conn->close();
        }else if($action == "del"){
            $matched_id = htmlspecialchars($_POST['matched_id']);
            $sql = "UPDATE `cb_match` SET `is_deleted` = 'true' WHERE `user_id` = '$id' AND `matched_id` = '$matched_id'";

            if ($conn->query($sql) === TRUE) {//If works..
                echo '{"response": "success"}';
            } else {//And if found any errors..
                echo '{"response": "'.$conn->error.'"}';
            }
            $conn->close();
        }else {
            echo '{"response":"action_not_found"}';
            $conn->close();
        }
    }
?>