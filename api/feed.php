<?php 
    /*
    *
    * This is the part of the API that is responsible for get the user match feed
    *
    */
    // Allow from any origin
    header('Access-Control-Allow-Origin: *');
    header("Content-type:multipart/form-data");
    header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-  Disposition, Content-Description');
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
    
    include_once 'conn.php'; //Include the connection with db.
    $conn = conn(); //Do the connection with server.

    //SET TIME ZONE FOR UTC
    date_default_timezone_set('America/Bahia');
    //Getting json code and converto to php data vector.
    $_POST = json_decode(file_get_contents('php://input'), true);

    if ($conn->connect_error) {
        die('{"response":"'.$conn->connect_error.'"}');
        $conn->close();
    }else{
        $id = htmlspecialchars($_POST['id']);
        $min = htmlspecialchars($_POST['min']);
        $max = htmlspecialchars($_POST['max']);
        $gender = htmlspecialchars($_POST['gender']);

        $sql = "SELECT * FROM `cb_configuration` WHERE `conf_key` = 'showme' AND `key_value` = 'true' ORDER BY RAND() LIMIT 10";
        $result = $conn->query($sql);
        
        if ($result && $result->num_rows <= 0) {
            echo '{"response": "no_one"}';
            $conn->close();
        } else if($result && $result->num_rows > 0){
            $users = '{"users":[';
            while($rowData = mysqli_fetch_array($result)) {
                $user_id = $rowData['user_id'];

                if($user_id != $id){
                    $sql = "SELECT * FROM `cb_match` WHERE `user_id` = '$id' AND `matched_id` = '$user_id'";
                    $r = $conn->query($sql);

                    if ($r && $r->num_rows <= 0) {//Verify if exist
                        $sql = "SELECT * FROM `cb_like` WHERE `user_id` = '$id' AND `user_liked` = '$user_id'";
                        $r = $conn->query($sql);

                        if ($r && $r->num_rows <= 0) {
                            $sql = "SELECT `key_value` FROM `cb_configuration` WHERE `conf_key` = 'gender' AND `user_id` = '$user_id'";
                            $r = $conn->query($sql);
                            $r = mysqli_fetch_array($r);
                            
                            if($r['key_value'] == $gender){
                                $sql = "SELECT `key_value` FROM `cb_configuration` WHERE `conf_key` = 'age' AND `user_id` = '$user_id'";
                                $r = $conn->query($sql);
                                $r = mysqli_fetch_array($r);
                                
                                if($r['key_value'] >= $min && $r['key_value'] <= $max){
                                    $users .= '{"id":"'.$user_id.'",';
                                    $sql = "SELECT `key_value` FROM `cb_configuration` WHERE `conf_key` = 'name' AND `user_id` = '$user_id'";
                                    $r = $conn->query($sql);
                                    $r = mysqli_fetch_array($r);
                                    $users .= '"name":"'.$r["key_value"].'",';
            
                                    $sql = "SELECT `key_value` FROM `cb_configuration` WHERE `conf_key` = 'lastname' AND `user_id` = '$user_id'";
                                    $r = $conn->query($sql);
                                    $r = mysqli_fetch_array($r);
                                    $users .= '"lastname":"'.$r["key_value"].'",';
            
                                    $sql = "SELECT `key_value` FROM `cb_configuration` WHERE `conf_key` = 'bio' AND `user_id` = '$user_id'";
                                    $r = $conn->query($sql);
                                    $r = mysqli_fetch_array($r);
                                    $users .= '"bio":"'.preg_replace("/\r|\n|\r\n/", '\n', $r["key_value"]).'",';

                                    $sql = "SELECT `key_value` FROM `cb_configuration` WHERE `conf_key` = 'school' AND `user_id` = '$user_id'";
                                    $r = $conn->query($sql);
                                    $r = mysqli_fetch_array($r);
                                    $users .= '"school":"'.preg_replace("/\r|\n|\r\n/", '\n', $r["key_value"]).'",';
            
                                    $sql = "SELECT `key_value` FROM `cb_configuration` WHERE `conf_key` = 'job' AND `user_id` = '$user_id'";
                                    $r = $conn->query($sql);
                                    $r = mysqli_fetch_array($r);
                                    $users .= '"job":"'.preg_replace("/\r|\n|\r\n/", '\n', $r["key_value"]).'",';
            
                                    $sql = "SELECT `key_value` FROM `cb_configuration` WHERE `conf_key` = 'work' AND `user_id` = '$user_id'";
                                    $r = $conn->query($sql);
                                    $r = mysqli_fetch_array($r);
                                    $users .= '"work":"'.preg_replace("/\r|\n|\r\n/", '\n', $r["key_value"]).'",';
            
                                    //Get Images from this user
                                    $sql = "SELECT `key_value` FROM `cb_configuration` WHERE `conf_key` = 'imageprofile' AND `user_id` = '$user_id'";
                                    $r = $conn->query($sql);
                                    $r = mysqli_fetch_array($r);
                                    $users .= '"image":[';
                                    $users .= '{"url":"'.$r["key_value"].'"}';

                                    $sql = "SELECT * FROM `cb_images` WHERE `user_id` = '$user_id' AND `is_deleted` = 'false'";
                                    $r = $conn->query($sql);
                                    
                                    while($rowDataI = mysqli_fetch_array($r)) {
                                        $img_str .= ',{"url":"'.$rowDataI["url"].'"}';
                                    }
                                    $users .= ']},';
                                }
                            }
                        }
                    }
                }
            }
            if (strpos($users, ',') !== false) {
                $users = substr_replace($users ,'', -1);
            }
            echo $users.'], "response":"success"}';
            $conn->close();
        }
    }
?>