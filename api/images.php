<?php 
    /*
    *
    * This is the part of the API that is responsible for upload, get and delete an image in the user profile
    *
    */
    // Allow from any origin
    header('Access-Control-Allow-Origin: *');
    header("Content-type:multipart/form-data");
    header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-  Disposition, Content-Description');
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");

    include_once 'conn.php'; //Include the connection with db.
    $conn = conn(); //Do the connection with server.

    //SET TIME ZONE FOR UTC
    date_default_timezone_set('America/Bahia');
    //Getting json code and converto to php data vector.
    $_POST = json_decode(file_get_contents('php://input'), true);

    //Check if the connection work.
    if ($conn->connect_error) {
        die('{"response":"'.$conn->connect_error.'"}');
        $conn->close();
    }else{
        //Getting action and user id
        $action = htmlspecialchars($_POST['action']);
        $id = htmlspecialchars($_POST['id']);
        //Make target dir
        $target_dir = 'user_images/'.$id.'/images/';
        //Use this to check if upload gone
        $uploadOk = 1;

        //Checking the action
        if($action == "get"){//This action get all user images, if "is_deleted" is false
            $value = "false";
            
            $sql = "SELECT * FROM `cb_images` WHERE `user_id` = '$id' AND `is_deleted` = '$value'";
            $result = $conn->query($sql);

            if ($result && $result->num_rows <= 0) {
                echo '{"response": "empty"}';
                $conn->close();
            } else if($result && $result->num_rows > 0){
                echo '{"image":[';
                while($rowData = mysqli_fetch_array($result)) {
                    echo '{"url":"'.$rowData["url"].'","id":"'.$rowData["id"].'"},';
                }
                echo '{"id":"0","url":"0"}],"response":"success"}';
                $conn->close();
            }else {
                echo '{"response": "'.$conn->error.'"}';
                $conn->close();
            }
        }else if($action == "del"){//This action delete a image with his id
            $image_id = htmlspecialchars($_POST['image_id']);
            $value = "true";//Value to get any deleted
            //Do the sql request
            $sql = "UPDATE `cb_images` SET `is_deleted` = '$value' WHERE `user_id` = '$id' AND `id` = '$image_id'";
            if ($conn->query($sql) === TRUE) {//If works..
                echo '{"response": "success"}';
                $conn->close();
            } else {//And if found any errors..
                echo '{"response": "'.$conn->error.'"}';
                $conn->close();
            }
        }else if($action == "profget"){
            $key = "imageprofile";
            $sql = "SELECT `key_value` FROM `cb_configuration` WHERE `user_id` = '$id' AND `conf_key` = '$key'";
            $result = $conn->query($sql);

            if ($result && $result->num_rows <= 0) {
                echo '{"response": "not_found"}';
                $conn->close();
            } else if($result && $result->num_rows > 0){
                $result = mysqli_fetch_array($result);
                echo '{"response":"success", "image":"'.$result["key_value"].'"}';
                    $conn->close();
            }else {
                echo '{"response": "'.$conn->error.'"}';
                $conn->close();
            }
        }else if($action == "profdel"){ 
            $key = "imageprofile";
            $value = "img/profile.png";//Value to get any deleted
            //Do the sql request
            $sql = "UPDATE `cb_configuration` SET `key_value` = '$value' WHERE `user_id` = '$id' AND `conf_key` = '$key'";

            if ($conn->query($sql) === TRUE) {//If works..
                echo '{"response": "success"}';
                $conn->close();
            } else {//And if found any errors..
                echo '{"response": "'.$conn->error.'"}';
                $conn->close();
            }
        }else{//If anyone action is not found
            echo '{"response":"action_not_found"}';
            $conn->close();
        }
    }
?>