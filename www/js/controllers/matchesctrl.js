angular.module('starter.controllers')
  .controller('MatchesCtrl', function ($scope, $http) {
    $scope.$on('$ionicView.enter', function () {

      $scope.newmatches = [];
      $scope.groups = [];

      $scope.getmatch = function () {
        $scope.loadshow();
        user_id = $scope.GetConfiguration("id");

        $scope.http_generator($scope.like_url,
          JSON.stringify({
            action: "get",
            id: user_id
          }), function (r) {
            response = JSON.stringify(r);
            response = JSON.parse(response);
            if (response.response !== "undefined" && response.response === "success") {
              for (i in response.match) {
                if (response.match[i].matched_id !== "0") {
                  $scope.newmatches.push({
                    name: response.match[i].name,
                    url: response.match[i].imageprofile,
                    id: response.match[i].matched_id,
                    new: response.match[i].notify === "true" ? "false" : "true"
                  });
                }
              }
            }
          });
      };

      $scope.getgroups = function () {
        $scope.loadshow();
        user_id = $scope.GetConfiguration("id");

        $scope.http_generator($scope.group_url,
          JSON.stringify({
            action: "get",
            id: user_id
          }), function (r) {
            response = JSON.stringify(r);
            response = JSON.parse(response);
            if (response.response !== "undefined" && response.response === "success") {
              for (i in response.groups) {
                if (response.groups[i].id !== "0") {
                  if (response.groups[i].message === '') {
                    $scope.groups.push({
                      name: response.groups[i].user_name,
                      lastmessage: "Mande uma mensagem!",
                      url: response.groups[i].user_image,
                      id: response.groups[i].user_two,
                      groupid: response.groups[i].id
                    });
                  } else {
                    $scope.groups.push({
                      name: response.groups[i].user_name,
                      lastmessage: response.groups[i].message.slice(0, 50),
                      url: response.groups[i].user_image,
                      id: response.groups[i].user_two,
                      groupid: response.groups[i].id
                    });
                  }
                }
              }
            }
          });
      };
      //
      // Get configurations
      $scope.gethttpconf();
      $scope.attpic();
      $scope.attprofpic();
      $scope.gettoprof();
      $scope.getmatch();
      $scope.getgroups();
    });
  });//MatchesCtrl
