// Inspired by Elastichat http://codepen.io/rossmartin/pen/XJmpQr
angular.module('starter.controllers')
  .controller('MessagingCtrl', function ($scope, $state, $ionicPopup, $stateParams, $ionicScrollDelegate, $ionicActionSheet, ionicDatePicker) {
    var week_day = ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"];
    var month = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];

    $scope.isnew = $stateParams.isNew;
    $scope.group_id = $stateParams.id;
    $scope.lastid = 0;

    $scope.user = {};

    $scope.messages = [];
    $scope.message = '';

    $scope.Closed = true;

    var viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');

    //Verify if User messages is enabled
    $scope.checkopen = function () {
      $scope.http_generator($scope.permi_url,
        JSON.stringify({
          action: "hour"
        }), function (r) {
          response = JSON.stringify(r);
          response = JSON.parse(response);
          if (response.response !== "undefined" && response.response === "opened") {
            $scope.Closed = false;
          }
        });
    };
    //Sending a Calendar Message to Other user
    $scope.openCalendar = function () {
      var ipObj1 = {
        callback: function (val) {
          date = new Date(val);
          date = week_day[date.getDay()] + ', dia ' + date.getDate() + ' de ' + month[date.getMonth()];
          //date = date.slice(0, 15);
          console.log('datepicker: ' + date.getDay, new Date(val));
          $scope.setmessages('Que tal nos conhecermos ' + date + ', no Crush Bar (Ville Gourmet)?');
        }
      };
      ionicDatePicker.openDatePicker(ipObj1);
    };
    //Do the scroll to bottom
    var _scrollBottom = function (target) {
      target = target || '#type-area';

      viewScroll.scrollBottom(true);
      _keepKeyboardOpen(target);
    };
    // Show Options
    $scope.showUserOptions = function () {
      $ionicActionSheet.show({
        buttons: [
          { text: 'Desfazer Match' },
          { text: 'Reportar Usuário' }
        ],
        buttonClicked: function (index) {
          if (index === 0) {
            $ionicPopup.alert({
              title: 'Desfazer Match',
              template: 'Tem certeza?',
              buttons: [
                { text: 'Cancelar' },
                {
                  text: 'Sim',
                  onTap: function () {
                    $scope.loadshow();
                    $scope.http_generator($scope.group_url,
                      JSON.stringify({
                        action: "del",
                        group_id: $scope.group_id
                      }), function (r) {
                        $state.go('home.matches', {}, { reload: true });
                      }, function () {
                        $scope.alert("Oh...", "Não conseguimos excluir o match.. Se quiser tente novamente.");
                      });
                  }
                }
              ]
            });
          } else if (index === 1) {
            $ionicPopup.alert({
              title: 'Suporte',
              template: 'Envie um e-mail, com o seu nome e o nome do usuário seguidos do motivo, para: ' + $scope.sup_email,
              buttons: [
                { text: 'Voltar' }]
            });
          }
        }
      });
    };
    //Send message to screen
    $scope.sendText = function (isMe, avatar, type, body, time) {
      $scope.messages.push({
        isMe: isMe,
        avatar: avatar,
        type: type,
        body: body,
        timestamp: time
      });
      $scope.message = '';
      _scrollBottom();
    };
    var _keepKeyboardOpen = function (target) {
      target = target || '#type-area';
      txtInput = angular.element(document.body.querySelector(target));
      txtInput.one('blur', function () {
        txtInput[0].focus();
      });
    };
    //Get Last Messages
    $scope.getlasts = function () {
      $scope.http_generator($scope.message_url,
        JSON.stringify({
          action: "lasts",
          group_id: $scope.group_id,
          mesid: $scope.lastid
        }), function (r) {
          response = JSON.stringify(r);
          response = JSON.parse(response);
          if (response.response !== "undefined" && response.response === "success") {
            for (i in response.messages) {
              if (response.messages[i].id === user_id) {
                $scope.sendText(true, '', 'text', response.messages[i].message, response.messages[i].date);
              } else {
                $scope.user = {
                  name: response.messages[i].user_name,
                  url: response.messages[i].user_image
                };
                $scope.sendText(false, response.messages[i].user_image, 'text', response.messages[i].message, response.messages[i].date);
              }
              $scope.lastid = response.messages[i].mes_id;
            }
          }
        });
    };
    //Set message on server
    $scope.setmessages = function (message) {
      $scope.message = '';
      user_id = $scope.GetConfiguration("id");
      if (message !== '') {
        if ($scope.isnew) {
          console.log(message);
          $scope.http_generator($scope.group_url,
            JSON.stringify({
              action: "set",
              id: user_id,
              user_two: $stateParams.id,
              message: message
            }), function (r) {
              response = JSON.stringify(r);
              response = JSON.parse(response);
              console.log(response.response);
              if (response.response !== "undefined" && response.response === "success") {
                $scope.group_id = response.group_id;
                $scope.lastid = response.mes_id;

                $scope.sendText(true, '', 'text', response.message, response.date);

                $scope.isnew = false;

                interval = window.setInterval(function () {
                  $scope.getlasts();
                  //$scope.checkopen();
                }, 3000);

                $scope.$on('$destroy', function () {
                  clearInterval(interval);
                });

              } else if (response.response !== "undefined" && response.response === "exist") {
                $scope.isnew = false;
                $scope.group_id = response.group_id;

                $scope.get_user(response.group_id);

                interval = window.setInterval(function () {
                  $scope.getlasts();
                  //$scope.checkopen();
                }, 3000);

                $scope.$on('$destroy', function () {
                  clearInterval(interval);
                });
              }
            });
        } else {
          $scope.http_generator($scope.message_url,
            JSON.stringify({
              action: "set",
              group_id: $scope.group_id,
              message: message,
              id: user_id
            }), function (r) {
              response = JSON.stringify(r);
              response = JSON.parse(response);
              if (response.response !== "undefined" && response.response === "success") {
                if (response.id === user_id) {
                  $scope.sendText(true, '', 'text', response.message, response.date);
                } else {
                  $scope.sendText(false, $scope.user.url, 'text', response.message, response.date);
                }
                $scope.lastid = response.mes_id;
              }
            });
        }
      }
    };
    //Create Message Group if Not Exist one for this users
    $scope.get_user = function (id) {
      $scope.http_generator($scope.conf_url,
        JSON.stringify({
          action: "get",
          id: id
        }), function (r) {
          response = JSON.stringify(r);
          response = JSON.parse(response);
          if (response.response !== "undefined" && response.response === "success") {
            $scope.user = {
              name: response.name,
              url: response.imageprofile
            };
          }
        });
    };
    $scope.newgroup = function () {
      if ($scope.isnew === true) {
        $scope.get_user($stateParams.id);
        
        if ($stateParams.Message !== '') {
          console.log("mensagem não vazia");
          $scope.setmessages($stateParams.Message);
        }
      } else {
        $scope.http_generator($scope.group_url,
          JSON.stringify({
            action: "getthis",
            group_id: $scope.group_id
          }), function (r) {
            console.log("chamou");
            response = JSON.stringify(r);
            response = JSON.parse(response);
            if (response.response !== "undefined" && response.response === "success") {
              if (response.user_one !== user_id) {
                $scope.get_user(response.user_one);
                console.log("primeiro if");
              } else if (response.user_two !== user_id) {
                $scope.get_user(response.user_two);
                console.log("segundo if");
              }
            }
          });


        interval = window.setInterval(function () {
          $scope.getlasts();
          //$scope.checkopen();
        }, 3000);

        $scope.$on('$destroy', function () {
          clearInterval(interval);
        });
      }
    };
    $scope.newgroup();
  });//MessagingCtrl
