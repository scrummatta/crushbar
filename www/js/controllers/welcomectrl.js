angular.module('starter.controllers', [])
  .controller('WelcomeCtrl', function ($scope, $ionicModal, $state, md5, localStorageService) {
    $scope.modals = function () {
      //Signup Modal
      $ionicModal.fromTemplateUrl('templates/modals/signup.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function (modal) {
        $scope.modalSignup = modal;
      });
      $scope.openSignupModal = function () {
        $scope.modalSignup.show();
      };
      $scope.closeSignupModal = function () {
        $scope.modalSignup.hide();
      };
      //Email Login Modal
      $ionicModal.fromTemplateUrl('templates/modals/login_form.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function (modal) {
        $scope.modalLoginForm = modal;
      });
      $scope.openLoginFormModal = function () {
        $scope.modalLoginForm.show();
      };
      $scope.closeLoginFormModal = function () {
        $scope.modalLoginForm.hide();
      };
      //Remove Modals from screen on $scope destroy
      $scope.$on('$destroy', function () {
        $scope.modalSignup.remove();
        $scope.modalLoginForm.remove();
      });
    };
    if ($scope.GetConfiguration("id") !== null) {
      $scope.loadshow();
      email = $scope.GetConfiguration("email");
      md5pass = md5.createHash($scope.GetConfiguration("password"));

      $scope.http_generator($scope.login_url,
        JSON.stringify({
          email: email,
          password: md5pass.toString()
        }), function (r) {
          response = JSON.stringify(r);
          response = JSON.parse(response);
          if (response.response !== "undefined" && response.response === "success") {
            $state.go('home.explore');
          } else {
            if (localStorageService.isSupported) {
              localStorageService.clearAll();
            } else if (localStorageService.cookie.isSupported) {
              localStorageService.cookie.clearAll();
            }
            $scope.alert("Oh...", "Tivemos um problema ao fazer o login automatico. Tente fazer o login novamente.");
            $scope.modals();
          }
        }, function () {
          if (localStorageService.isSupported) {
            localStorageService.clearAll();
          } else if (localStorageService.cookie.isSupported) {
            localStorageService.cookie.clearAll();
          }
          $scope.alert("Oh...", "Não conseguimos fazer o login automatico. Faça login novamente, recomendamos contactar nosso suporte caso não consiga.");
          $scope.modals();
        });
    } else {
      $scope.modals();
    }
  });//WelcomeCtrl
