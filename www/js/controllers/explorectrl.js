angular.module('starter.controllers')
  .controller('ExploreCtrl', function ($scope, $ionicModal, $ionicSlideBoxDelegate) {
    user_id = $scope.GetConfiguration("id");
    // Tinder cards
    var cards = [];
    $scope.cards = [];
    $scope.matchcard = [];
    $scope.supermatchcard = [];

    function _perfilAdjust() {
      if ($scope.GetConfiguration("see") === null || $scope.GetConfiguration("see") === false) {
        $scope.SaveLConfiguration("see", true);
        if ($scope.tel === '' || $scope.name === '' || $scope.lastname === ''
          || $scope.job === '' || $scope.work === '' || $scope.bio === ''
          || $scope.school === '') {
          $scope.alert('Fique Ligado!', 'Preencha todos os dados do seu perfil para ter mais chances! Não esqueça de colocar algumas fotos.');
        }
      }
    }

    function _addCards(quantity) {
      for (var i = 0; i < Math.min(cards.length, quantity); i++) {
        $scope.cards.push(cards[0]);
        cards.splice(0, 1);
      }
      if (cards.length <= 1) {
        $scope.attfeed();
      }
    }

    $scope.cardNope = function (index) {

      $scope.cards.splice(index, 1);
      _addCards(1);

      $scope.isMoveLeft = false;
      $scope.isMoveRight = false;

      $scope.closeFeedProfModal();
    };
    $scope.cardBeer = function (index) {
      $scope.http_generator($scope.like_url,
        JSON.stringify({
        action: "set",
        id: user_id,
        like_id: $scope.cards[0].id.toString(),
        like_type: "superlike"
      }), function () {
          $scope.alert("Sistema", "Você acaba de enviar um drink.");
        });

      $scope.cards.splice(index, 1);
      _addCards(1);

      $scope.isMoveLeft = false;
      $scope.isMoveRight = false;

      $scope.closeFeedProfModal();
    };
    $scope.cardYep = function (index) {
      $scope.http_generator($scope.like_url,
        JSON.stringify({
        action: "set",
        id: user_id,
        like_id: $scope.cards[0].id.toString(),
        like_type: "like"
        }), function (r) {
          response = JSON.stringify(r);
          response = JSON.parse(response);
          if (response.response !== "undefined" && response.response === "liked") {
            if (response.like_type === "like") {
              $scope.matchcard = $scope.cards[0];
              $scope.openMatchModal();
            }
            $scope.updmatch("upd", $scope.cards[0].id);

            
          }
        });

      $scope.cards.splice(index, 1);
      _addCards(1);

      $scope.isMoveLeft = false;
      $scope.isMoveRight = false;

      $scope.closeFeedProfModal();
    };
    $scope.cardSwipedLeft = function (event, index) {
      event.stopPropagation();
      $scope.cardNope(index);
    };
    $scope.cardSwipedRight = function (event, index) {
      event.stopPropagation();
      $scope.cardYep(index);
    };
    //
    $scope.cardPartialSwipe = function (amt) {
      $scope.isMoveLeft = amt < -0.15;
      $scope.isMoveRight = amt > 0.15;
    };
    
    $scope.getfeed = function (min, max, gender) {
      if (cards.length <= 1) {
        $scope.http_generator($scope.feed_url,
          JSON.stringify({
            id: user_id,
            min: min.toString(),
            max: max.toString(),
            gender: gender.toString()
          }), function (r) {
            response = JSON.stringify(r);
            response = JSON.parse(response);
            if (response.response !== "undefined" && response.response === "success") {
              for (i in response.users) {
                if (response.users[i].id !== "0") {
                  cards.push(response.users[i]);
                }
              }
              if ($scope.cards.length <= 1) {
                _addCards(2);
              }
            }
          });
      } else {
        $scope.addToCard();
      }
    };
    $scope.attfeed = function () {
      if ($scope.choicewoman === true && typeof ($scope.choicewoman) !== 'undefined') {
        $scope.getfeed($scope.barProgressMin, $scope.barProgressMax, 0);
      }
      if ($scope.choiceman === true && typeof ($scope.choiceman) !== 'undefined') {
        $scope.getfeed($scope.barProgressMin, $scope.barProgressMax, 1);
      }
    };
    $scope.getlastmatch = function () {
      $scope.http_generator($scope.like_url,
        JSON.stringify({
          action: "get",
          id: user_id
        }), function (r) {
          response = JSON.stringify(r);
          response = JSON.parse(response);
          if (response.response !== "undefined" && response.response === "success") {
            for (i in response.match) {
              if (response.match[i].notify === "false") {
                if (response.match[i].type === "like") {
                  $scope.matchcard = {
                    id: response.match[i].matched_id,
                    name: response.match[i].name,
                    image: [{ url: response.match[i].imageprofile }]
                  };
                  $scope.openMatchModal();
                } else if (response.match[i].type === "superlike") {
                  $scope.supermatchcard = {
                    id: response.match[i].matched_id,
                    name: response.match[i].name
                  };
                  $scope.openSuperMatchModal();
                }
                $scope.updmatch("upd", response.match[i].matched_id);
                break;
              }
            }
          }
        });
    };
    /* Modals */
    //Profile Feed Modal
    $ionicModal.fromTemplateUrl('templates/modals/feed_profile.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.feedProfModal = modal;
    });

    $scope.openFeedProfModal = function () {
      $ionicSlideBoxDelegate.slide(0, 300);
      $scope.feedProfModal.show();
    };
    $scope.closeFeedProfModal = function () {
      $scope.feedProfModal.hide();
      $ionicSlideBoxDelegate.slide(0, 300);
    };
    // Match popup
    $ionicModal.fromTemplateUrl('templates/modals/match.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.matchModal = modal;
    });
    $scope.openMatchModal = function () {
      $scope.matchModal.show();
    };
    $scope.closeMatchModal = function () {
      $scope.matchModal.hide();
    };
    // Super Match popup
    $ionicModal.fromTemplateUrl('templates/modals/super_match.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.superMatchModal = modal;
    });
    $scope.openSuperMatchModal = function () {
      $scope.superMatchModal.show();
    };
    $scope.closeSuperMatchModal = function () {
      $scope.superMatchModal.hide();
    };
    //
    //Toggle from top bar menu
    $scope.explore = {
      toggle: 0 // 0: Single; 1: Group
    };
    //
    // Get configurations
    $scope.gethttpconf();
    $scope.attpic();
    $scope.attprofpic();
    $scope.gettoprof();
    $scope.attfeed();

    interval = window.setInterval(function () {
      $scope.getlastmatch();
      _perfilAdjust();
    }, 10000);
    $scope.$on('$destroy', function () {
      clearInterval(interval);
    });
    
  });//ExploreCtrl
