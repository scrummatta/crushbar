angular.module('starter.controllers')
  .controller('SignupCtrl', function ($scope, md5, $state) {

    //e-mail check error
    $scope.emailproblem = 1;

    //Age range Controller
    $scope.barProgressMin = 20;
    $scope.barProgressMax = 40;

    //Gender controllers
    gender = 0; // Gender is 0 or 1.
    //Gender toggle creator and controller
    $scope.GenderList = [
      { text: "Mulher", checked: true, value: 0 },
      { text: "Homem", checked: false, value: 1 }
    ];
    //Add conf key on DB
    $scope.SetConfigKey = function (id, key, value) {
      $scope.http_generator($scope.conf_url,
        JSON.stringify({
          action: "set",
          id: id,
          key: key,
          value: value
        }), function (r) {
          response = JSON.stringify(r);
          response = JSON.parse(response);
          if (response.response !== "undefined" && response.response === "success") {
            return true;
          } else {
            return false;
          }
        });
    };
    // LoginForm
    $scope.LoginForm = function (e, p) {
      $scope.loadshow();
      //Form handling only after Login button is clicked.
      if (p === "undefined" || e === "undefined") {
        $scope.loadhide();
        $scope.alert("Opa!", "Você precisa preencher os campos corretamente.");
      } else if (e.indexOf('@') === -1 || e.indexOf('.') === -1) {
        $scope.loadhide();
        $scope.alert("Opa!", "Informe um e-mail válido!");
      } else {
        md5pass = md5.createHash(p);

        $scope.http_generator($scope.login_url,
          JSON.stringify({
            email: e.toString(),
            password: md5pass.toString()
          }), function (r) {
            response = JSON.stringify(r);
            response = JSON.parse(response);
            if (response.response !== "undefined" && response.response === "success") {
              $scope.SaveLogin(e, p, response.id);

              $scope.SaveLConfiguration("newmatches", true);
              $scope.SaveLConfiguration("message", true);
              $scope.SaveLConfiguration("drinks", true);
              $scope.SaveLConfiguration("notifpush", true);
              $scope.SaveLConfiguration("notifmail", true);

              $state.go('home.explore');
            } else if (response.response !== "undefined" && response.response === "not_found") {
              $scope.alert("Oh...", "Tem certeza que digitou o e-mail e/ou senha corretos?");
            } else {
              $scope.alert("Oh...", "001 - Acho que tivemos um problema... Verifique sua conexão e tente novamente");
            }
          });
      }
    };
    // /LoginForm
    //Age Range Selector controllers.
    $scope.onMin = function (e) {
      value = parseInt((e.target.max / e.target.offsetWidth) * (e.gesture.touches[0].screenX - e.target.offsetLeft));

      if (value > $scope.barProgressMax) {
        value = $scope.barProgressMax - 5;
      } else if (value < e.target.min) {
        value = e.target.min;
      }
      $scope.barProgressMin = value;
    };
    $scope.onMax = function (e) {
      value = parseInt((e.target.max / e.target.offsetWidth) * (e.gesture.touches[0].screenX - e.target.offsetLeft));

      if (value < $scope.barProgressMin) {
        value = $scope.barProgressMin + 5;
      } else if (value > e.target.max) {
        value = e.target.max;
      }
      $scope.barProgressMax = value;
    };
    //True/False switch controller.
    $scope.GenderChange = function (item) {
      aux = item.checked;
      for (var i = 0; i < 2; ++i) {
        $scope.GenderList[i].checked = !aux;
        if (!aux === true) {
          if ($scope.GenderList[i].value !== item.value) {
            gender = $scope.GenderList[i].value;
          }
        }
        else {
          gender = item.value;
        }
      }
      item.checked = aux;
    };
    //Basic phone check before confirm button.
    $scope.checktel = function (value) {
      if (typeof(value) !== "undefined") {
        if (value === '11111111111' || value === '22222222222' || value === '33333333333' || value === '44444444444'
          || value === '55555555555' || value === '66666666666' || value === '77777777777' || value === '88888888888'
          || value === '99999999999' || value === '00000000000') {
          $scope.alert("Opa!", "Informe um telefone válido!");
        } else if (value.indexOf('+') !== -1 || value.indexOf(')') !== -1 || value.indexOf('(') !== -1
          || value.indexOf('-') !== -1 || value.indexOf('/') !== -1 || value.indexOf('\\') !== -1) {
          $scope.alert("Opa!", "Use somente numeros!");
        }
      }
    };
    //check age less 18 or impossible old. above 122 calls a little joke.
    $scope.checkage = function (value) {
      if (typeof(value) !== "undefined") {
        if (value < 18) {
          $scope.alert("Opa!", "Desculpe, a idade minima é 18 anos!");
        } else if (value > 122) {
          $scope.alert("Opa!", "A pessoa mais velha do mundo teve 122 anos...");
        }
      }
    };
    //Basic email check before/after confirm button - using CrushbarAPI
    $scope.checkemail = function (value) {
      if (typeof(value) !== "undefined") {
        if (value.indexOf('@') === -1 || value.indexOf('.') === -1) {
          $scope.emailproblem = 1;
        } else {
          $scope.http_generator($scope.checkmail_url,
            JSON.stringify({
              email: value
            }), function (r) {
              response = JSON.stringify(r);
              response = JSON.parse(response);
              if (response.response === "exist") {
                $scope.emailproblem = 1;
                $scope.alert("Opa!", " Esse e-mail já foi cadastrado no nosso app. Utilize outro.");
              } else if (response.response !== "success") {
                $scope.emailproblem = 1;
              } else {
                $scope.emailproblem = 0;
              }
            }, function () {
              $scope.emailproblem = 1;
            });
        }
      } else {
        $scope.emailproblem = 1;
      }
    };
    //Checking equal pass and confirmation pass before confirm button.
    $scope.checkpass = function (value) {
      if (typeof(value) !== "undefined") {
        if ($scope.pass !== value) {
          $scope.loadhide();
          $scope.alert("Opa!", "As senhas precisam ser iguais!");
        }
      }
    };
    //Signup/Login with email - using CrushbarAPI
    $scope.confirmSignup = function () {
      $scope.loadshow();
      //Form handling after click "confirm" button
      if (typeof($scope.name) === "undefined") {
        $scope.loadhide();
        $scope.alert("Opa!", "Acho que esqueceu do Nome!");
      } else if (typeof($scope.lastname) === "undefined") {
        $scope.loadhide();
        $scope.alert("Opa!", "Acho que esqueceu seu Sobrenome!");
      } else if (typeof($scope.tel) === "undefined") {
        $scope.loadhide();
        $scope.alert("Opa!", "Acho que esqueceu do telefone!");
      } else if (typeof($scope.age) === "undefined") {
        $scope.loadhide();
        $scope.alert("Opa!", "Acho que esqueceu da sua idade!");
      } else if (typeof($scope.email) === "undefined") {
        $scope.loadhide();
        $scope.alert("Opa!", "Acho que esqueceu o e-mail!");
      } else if ($scope.choiceman === false && this.choicewoman === false) {
        $scope.loadhide();
        $scope.alert("Opa!", "Precisamos que escolha um genero de preferência...");
      } else if (typeof(this.pass) === "undefined" || typeof($scope.cpass) === "undefined") {
        $scope.loadhide();
        $scope.alert("Opa!", "Precisa definir uma senha.");
      } else {

        if (typeof ($scope.choicewoman) === "undefined") {
          $scope.choicewoman = false;
        }
        if (typeof ($scope.choiceman) === "undefined") {
          $scope.choiceman = false;
        }

        md5pass = md5.createHash($scope.pass);

        $scope.http_generator($scope.sig_url,
          JSON.stringify({
            email: $scope.email.toString(),
            password: md5pass.toString(),
            version_db: "0",
            name: $scope.name,
            lastname: $scope.lastname,
            school: "\n",
            job: "\n",
            work: "\n",
            bio: "\n",
            rangemin: $scope.barProgressMin.toString(),
            rangemax: $scope.barProgressMax.toString(),
            age: $scope.age.toString(),
            gender: gender.toString(),
            tel: $scope.tel.toString(),
            choicewoman: $scope.choicewoman.toString(),
            choiceman: $scope.choiceman.toString(),
            showme: "true",
            showmyage: "true",
            imageprofile: $scope.defprof,
            locationlat: "0",
            locationlong: "0"
          }), function (r) {
            response = JSON.stringify(r);
            response = JSON.parse(response);
            console.log(response);
            if (response.response !== "undefined" && response.response === "success") {//To Login

              $scope.LoginForm($scope.email, $scope.pass);

            } else if (response.response !== "undefined" && response.response === "exist") {
              $scope.alert("Oh...", "Esse e-mail já foi cadastrado. Tente outro, caso a conta tenha sido excluída, entre em contato com o suporte.");
            } else {
              $scope.alert("Oh...", "011 - Acho que tivemos um problema... Verifique sua conexão e tente novamente");
            }
          }, function () {
            $scope.alert("Oh...", "012 - Acho que tivemos um problema... Verifique sua conexão e tente novamente");
          });
      }
    };
  });//SignupCtrl
