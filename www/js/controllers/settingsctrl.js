angular.module('starter.controllers')
  .controller('SettingsCtrl', function ($scope, $state, localStorageService, $ionicModal, $ionicPopup) {
    // Modals Constructors
    $ionicModal.fromTemplateUrl('templates/modals/settings.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modalSettings = modal;
      });
    //Remove Modals from screen on $scope destroy
    $scope.$on('$destroy', function () {
      $scope.modalSettings.remove();
    });
    //Profile
    $scope.insta = "Em breve!";
    //Set or Delete profile image
    $scope.profpick = function () {
      //Get stored user ID
      user_id = $scope.GetConfiguration("id");
      if ($scope.ImgList[0].number === 0) {
        document.addEventListener("deviceready", function () {
          //Show loader
          $scope.loadshow();
          console.log("Profile Photo");
          //Open camera
          navigator.camera.getPicture(function (imageData) {
            //FileTransfer Object
            var ft = new FileTransfer();
            //File Name
            filename = new Date();
            //Make a custom name to photo, in this case, the complete hour(HH:MM:SS)
            filename = filename.getHours() + "-" + (filename.getMinutes() + 1) + "-" + filename.getSeconds() + ".jpg";
            //Params
            var params = new Object();
            params.id = user_id;
            params.action = "profset";
            //File Upload Options
            let options = new FileUploadOptions();
            options.fileKey = "photo";
            options.fileName = filename;
            options.mimeType = "image/jpeg";
            options.chunkedMode = false;
            options.params = params;
            options.headers = { Connection: "close" };
            //Send image to server and after get from server and set on imgSrc
            ft.upload(imageData, $scope.upl_img_url,
              function (data) {
                response = JSON.parse(data.response);
                if (response.response !== "undefined" && response.response === "success") {
                  console.log("SEND_WORK");
                  $scope.ImgList[0].src = response.url;
                  $scope.ImgList[0].number = 1;
                  $scope.ImgList[0].icon = 'icon ion-close-circled photo-button assertive light-bg text-2x rounded';
                } else {
                  console.log("SEND_NOT_SUCCESS");
                  $scope.alert("Oh...", "Não conseguimos salvar sua foto :(");
                }
                //Close loader after the job
                $scope.loadhide();
              },
              function (err) {
                $scope.loadhide();
                $scope.alert("Oh...", "Não conseguimos salvar sua foto :(");
                console.log("SEND_ERR");
                console.log(err);
              },
              options);
          }, function (err) {
            // error
            $scope.loadhide();
            $scope.alert("Oh...", "Tivemos um problema com sua camera...");

            console.log("CAM_ERR");
            console.log(err);
          },
            {
              quality: 60,
              destinationType: navigator.camera.DestinationType.FILE_URI,
              sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
              allowEdit: true,
              encodingType: navigator.camera.EncodingType.JPEG,
              targetWidth: 640,
              targetHeight: 640,
              popoverOptions: CameraPopoverOptions,
              saveToPhotoAlbum: false
            });
        }, false);
      } else {//Delete profile image if the number param is different of 0
        console.log("Delete Profile Photo");
        $ionicPopup.alert({
          title: 'Excluir',
          template: 'Tem certeza?',
          buttons: [
            { text: 'Cancelar' },
            {
              text: 'Sim',
              onTap: function () {
                $scope.loadshow();
                $scope.http_generator($scope.img_url,
                  JSON.stringify({
                    action: "profdel",
                    id: user_id
                  }), function (r) {
                    response = JSON.stringify(r);
                    response = JSON.parse(response);
                    if (response.response !== "undefined" && response.response === "success") {
                      $scope.ImgList[0].number = 0;
                      $scope.ImgList[0].src = $scope.defprof;
                      $scope.ImgList[0].icon = 'icon ion-plus-circled photo-button assertive light-bg text-2x rounded';
                    }
                  }, function () {
                    $scope.alert("Oh...", "Vish.. Não conseguimos deletar sua foto!");
                  });
              }
            }
          ]
        });
      }
    };
    //Set or Delete any others images
    $scope.pick = function (i) {
      //Get stored user ID
      user_id = $scope.GetConfiguration("id");
      //If have or not a photo
      if ($scope.ImgList[i].number === 0) {
        //Show loader
        $scope.loadshow();
        document.addEventListener("deviceready", function () {
          console.log("Any Photo");
          //Open camera
          navigator.camera.getPicture(function (imageData) {
            //For get base64
            //base64img = "data:image/jpeg;base64," + imageData;
            //FileTransfer Object
            var ft = new FileTransfer();
            //File Name
            filename = new Date();
            //Make a custom name to photo, in this case, the complete hour(HH:MM:SS)
            filename = filename.getHours() + "-" + (filename.getMinutes() + 1) + "-" + filename.getSeconds() + ".jpg";
            //Params
            var params = new Object();
            params.id = user_id;
            params.action = "set";
            //File Upload Options
            let options = new FileUploadOptions();
            options.fileKey = "photo";
            options.fileName = filename;
            options.mimeType = "image/jpeg";
            options.chunkedMode = false;
            options.params = params;
            options.headers = { Connection: "close" };
            //Send image to server and after get from server and set on imgSrc
            ft.upload(imageData, $scope.upl_img_url,
              function (data) {
                response = JSON.parse(data.response);
                if (response.response !== "undefined" && response.response === "success") {
                  console.log("SEND_WORK");
                  $scope.ImgList[i].src = response.url;
                  $scope.ImgList[i].number = response.id;
                  $scope.ImgList[i].icon = 'icon ion-close-circled photo-button assertive light-bg text-2x rounded';
                } else {
                  console.log("SEND_NOT_SUCCESS");
                  $scope.alert("Oh...", "Não conseguimos salvar sua foto :(");
                }
                //Close loader after the job
                $scope.loadhide();
              },
              function (err) {
                $scope.loadhide();
                console.log("SEND_ERR");
                $scope.alert("Oh...", "Não conseguimos salvar sua foto :(");
                console.log(err);
              },
              options);
          }, function (err) {
            // error
            $scope.loadhide();
            console.log("CAM_ERR");
            $scope.alert("Oh...", "Tivemos um problema com sua camera...");
            console.log(err);
          },
            {
              quality: 60,
              destinationType: navigator.camera.DestinationType.FILE_URI,
              sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
              allowEdit: true,
              encodingType: navigator.camera.EncodingType.JPEG,
              targetWidth: 640,
              targetHeight: 640,
              popoverOptions: CameraPopoverOptions,
              saveToPhotoAlbum: false
            });
        }, false);
      } else {//If the number is not 0 is because have a photo id in there, so if you click then is for delete the photo.
        console.log("Delete Photo");
        $ionicPopup.alert({
          title: 'Excluir',
          template: 'Tem certeza?',
          buttons: [
            { text: 'Cancelar' },
            {
              text: 'Sim',
              onTap: function () {
                $scope.loadshow();
                $scope.http_generator($scope.img_url,
                  JSON.stringify({
                    action: "del",
                    id: user_id,
                    image_id: $scope.ImgList[i].number
                  }), function (r) {
                    response = JSON.stringify(r);
                    response = JSON.parse(response);

                    if (response.response !== "undefined" && response.response === "success") {
                      $scope.ImgList[i].number = 0;
                      $scope.ImgList[i].src = $scope.defprof;
                      $scope.ImgList[i].icon = 'icon ion-plus-circled photo-button assertive light-bg text-2x rounded';
                    }
                  }, function () {
                    $scope.alert("Oh...", "Vish.. Não conseguimos deletar sua foto!");
                  });
              }
            }
          ]
        });
      }
    };
    //Settings
    $scope.onMin = function (e) {
      value = parseInt((e.target.max / e.target.offsetWidth) * (e.gesture.touches[0].screenX - e.target.offsetLeft));
      if (value > $scope.barProgressMax) {
        value = $scope.barProgressMax - 5;
      } else if (value < e.target.min) {
        value = e.target.min;
      }
      $scope.barProgressMin = value;
      $scope.updhttpconf("rangemin", value);
    };
    $scope.onMax = function (e) {
      value = parseInt((e.target.max / e.target.offsetWidth) * (e.gesture.touches[0].screenX - e.target.offsetLeft));
      if (value < $scope.barProgressMin) {
        value = $scope.barProgressMin + 5;
      } else if (value > e.target.max) {
        value = e.target.max;
      }
      $scope.barProgressMax = value;
      $scope.updhttpconf("rangemax", value);
    };
    //Gender controller.
    $scope.GenderChange = function (item) {
      aux = item.checked;
      for (var i = 0; i < 2; ++i) {
        $scope.GenderList[i].checked = !aux;
        if (!aux === true) {
          if ($scope.GenderList[i].value !== item.value) {
            $scope.gender = $scope.GenderList[i].value;
            $scope.updhttpconf("gender", $scope.gender);
          }
        }
        else {
          $scope.gender = item.value;
          $scope.updhttpconf("gender", $scope.gender);
        }
      }
      item.checked = aux;
    };
    //Basic phone check.
    $scope.checktel = function (value) {
      if (typeof (value) !== "undefined") {
        if (value === '11111111111' || value === '22222222222' || value === '33333333333' || value === '44444444444'
          || value === '55555555555' || value === '66666666666' || value === '77777777777' || value === '88888888888'
          || value === '99999999999' || value === '00000000000') {
          $scope.alert("Opa!", "Informe um telefone válido!");
        } else if (value.indexOf('(') !== -1 || value.indexOf(')') !== -1 || value.indexOf('+') !== -1
          || value.indexOf('-') !== -1 || value.indexOf('/') !== -1 || value.indexOf('\\') !== -1) {
          $scope.alert("Opa!", "Use somente numeros!");
        } else if (value.length === 11) {
          $scope.updhttpconf("tel", value);
        }
      }
    };
    //check age less 18 or impossible old. above 122 calls a little joke.
    $scope.checkage = function (value) {
      if (typeof (value) !== "undefined") {
        if (value < 18) {
          $scope.alert("Opa!", "Desculpe, a idade minima é 18 anos!");
        } else if (value > 122) {
          $scope.alert("Opa!", "A pessoa mais velha do mundo teve 122 anos...");
        } else {
          $scope.updhttpconf("age", value);
        }
      }
    };
    //Logout
    $scope.logout = function () {
      $ionicPopup.alert({
        title: 'Sair',
        template: 'Tem certeza que deseja sair da sua conta? Seu cadastro ainda estará disponível para que possa entrar novamente.',
        buttons: [
          {
            text: 'Cancelar',
            type: 'button-assertive'
          },
          {
            text: 'Sim',
            type: 'button-energized',
            onTap: function () {
              $scope.loadshow();

              $scope.gender = 0;
              $scope.choicewoman = false;
              $scope.choiceman = false;
              $scope.age = 18;
              $scope.showme = 1;
              $scope.showmyage = 1;
              $scope.barProgressMin = 20;
              $scope.barProgressMax = 30;
              $scope.tel = '';
              $scope.name = '';
              $scope.lastname = '';
              $scope.imageprofile = '';
              $scope.job = '';
              $scope.work = '';
              $scope.bio = '';
              $scope.school = '';

              if (localStorageService.isSupported) {
                localStorageService.clearAll();
              } else if (localStorageService.cookie.isSupported) {
                localStorageService.cookie.clearAll();
              }

              $scope.loadhide();
              $state.go('welcome');
            }
          }
        ]
      });
    };
    $scope.deleteacc = function () {
      user_id = $scope.GetConfiguration("id");

      $ionicPopup.alert({
        title: 'Deletar Conta',
        template: 'Tem certeza que deseja excluir sua conta? Este email não poderá mais ser usado para cadastro aqui.',
        buttons: [
          {
            text: 'Cancelar',
            type: 'button-assertive'
          },
          {
            text: 'Sim',
            type: 'button-energized',
            onTap: function () {
              $scope.loadshow();
              $scope.http_generator($scope.del_user,
                JSON.stringify({
                  id: user_id
                }), function (r) {
                  response = JSON.stringify(r);
                  response = JSON.parse(response);
                  if (response.response !== "undefined" && response.response === "success") {
                    if (localStorageService.isSupported) {
                      localStorageService.clearAll();
                    } else if (localStorageService.cookie.isSupported) {
                      localStorageService.cookie.clearAll();
                    }
                    $state.go('welcome');
                  }
                }, function () {
                  $scope.alert("Oh...", "Verifique sua conexão!");
                });
            }
          }
        ]
      });
    };

    $scope.suport = function () {
      $ionicPopup.alert({
        title: 'Suporte',
        template: 'Envie um e-mail para ' + $scope.sup_email + '. Lembre-se de usar o mesmo e-mail usado aqui no Crush Bar App.',
        buttons: [
          { text: 'Voltar' }]
      });
    };

    //Do "open" here for take gethttpconf function on execute the modal
    $scope.openSettingsModal = function () {
      $scope.gethttpconf();
      $scope.attpic();
      $scope.attprofpic();
      $scope.gettoprof();
      $scope.modalSettings.show();
    };
    $scope.closeSettingsModal = function () {
      $scope.gethttpconf();
      $scope.attpic();
      $scope.attprofpic();
      $scope.gettoprof();
      $scope.modalSettings.hide();
    };
    //Execute get configurations to show on configuration modal.
    $scope.gethttpconf();
    $scope.attpic();
    $scope.attprofpic();
    $scope.gettoprof();
  });//SettingsCtrl
