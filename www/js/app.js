// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', [
  'ionic',
  'starter.controllers',
  'starter.services',
  'starter.directives',
  'ionic.contrib.ui.tinderCards',
  'ionic.giphy',
  'ngCordova',
  'ion-sticky',
  'angular-md5',
  'LocalStorageModule',
  'ionic-datepicker'
])
  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
        $ionicConfigProvider.scrolling.jsScrolling(false);
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });
  })
  .config(function ($ionicConfigProvider) {
    $ionicConfigProvider.navBar.alignTitle('center');
    $ionicConfigProvider.views.swipeBackEnabled(false);
  })
  .config(function (localStorageServiceProvider) {
    localStorageServiceProvider
      .setPrefix('starter')
      .setStorageCookie(45, '/', false);
  })
  .config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/welcome');
    //Testar Splash mudando o router
    $stateProvider
      .state('welcome', {
        url: '/welcome',
        templateUrl: 'templates/welcome/welcome.html',
        controller: 'WelcomeCtrl'
      })
      .state('home', {
        url: '/home',
        abstract: true,
        templateUrl: 'templates/home/index.html'
      })
      .state('home.explore', {
        url: '/explore',
        templateUrl: 'templates/home/explore.html',
        controller: 'ExploreCtrl'
      })
      .state('home.settings', {
        url: '/settings',
        templateUrl: 'templates/home/settings.html',
        controller: 'SettingsCtrl'
      })
      .state('home.matches', {
        url: '/matches',
        templateUrl: 'templates/home/matches.html',
        controller: 'MatchesCtrl'
      })
      .state('home.messaging', {
        url: '/messaging',
        params: { id: 0, isNew: true, Message: '' },
        templateUrl: 'templates/home/messaging.html',
        controller: 'MessagingCtrl'
      });
  })
  .config(function (ionicDatePickerProvider) {
    var datePickerObj = {
      inputDate: new Date(),
      titleLabel: 'Escolha a Data',
      setLabel: 'Enviar',
      todayLabel: 'Hoje',
      closeLabel: 'Sair',
      mondayFirst: false,
      weeksList: ["Seg", "Ter", "Qua", "Qui", "Sex", "Sab", "Dom"],
      monthsList: ["Jan", "Feb", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
      templateType: 'popup',
      from: new Date(2019, 10, 11),
      to: new Date(2022, 1, 1),
      showTodayButton: true,
      dateFormat: 'dd MMMM yyyy',
      closeOnSelect: false,
      disableWeekdays: []
    };
    ionicDatePickerProvider.configDatePicker(datePickerObj);
  })
  //Global functions and variables
  .run(function ($rootScope, $state, localStorageService, $ionicLoading, $ionicModal, $http, $ionicPopup, $ionicSlideBoxDelegate) {
    //Is beta switch
    $rootScope.see = false;
    is_beta = false;
    bar_url = is_beta === true ? 'http://localhost/bar/app/crushbar/api/beta/' :'https://www.bardapaquera.com.br/api/';
    
    $rootScope.sup_email = 'suporte@bardapaquera.com.br';
    //HTTP URL variables
    $rootScope.conf_url = bar_url + 'configurations.php';
    $rootScope.login_url = bar_url + 'login.php';
    $rootScope.checkmail_url = bar_url + 'user_verify.php';
    $rootScope.sig_url = bar_url + 'signup.php';
    $rootScope.upl_img_url = bar_url + 'upload_image.php';
    $rootScope.img_url = bar_url + 'images.php';
    $rootScope.terms_url = bar_url + 'docs/termos-de-servico.pdf';
    $rootScope.priv_url = bar_url + 'docs/privacidade.pdf';
    $rootScope.del_user = bar_url + 'delete.php';
    $rootScope.feed_url = bar_url + 'feed.php';
    $rootScope.like_url = bar_url + 'liked.php';
    $rootScope.group_url = bar_url + 'group.php';
    $rootScope.message_url = bar_url + 'message.php';
    $rootScope.permi_url = bar_url + 'get_permission.php';
    //HTTP default headers
    $rootScope.headers = {"Access-Control-Allow-Origin": "*"};
    //Sistem Variables
    $rootScope.done_beer = 'Aceito um drink!';
    $rootScope.here_im = 'Estou no Bar. Venha me encontrar!';
    $rootScope.match_date = 'Vamos marcar um encontro no Crush Bar?';
    //Input Variables
    $rootScope.defprof = 'img/profile.png';
    $rootScope.ImgList = [{ number: 0, src: $rootScope.defprof, icon: 'icon ion-plus-circled photo-button assertive light-bg text-2x rounded' },
      { number: 0, src: $rootScope.defprof, icon: 'icon ion-plus-circled photo-button assertive light-bg text-2x rounded' },
      { number: 0, src: $rootScope.defprof, icon: 'icon ion-plus-circled photo-button assertive light-bg text-2x rounded' },
      { number: 0, src: $rootScope.defprof, icon: 'icon ion-plus-circled photo-button assertive light-bg text-2x rounded' },
      { number: 0, src: $rootScope.defprof, icon: 'icon ion-plus-circled photo-button assertive light-bg text-2x rounded' },
      { number: 0, src: $rootScope.defprof, icon: 'icon ion-plus-circled photo-button assertive light-bg text-2x rounded' }];

    $rootScope.ProfList = [];

    $rootScope.gender = 0;
    $rootScope.GenderList = [
      { text: "Mulher", checked: true, value: 0 },
      { text: "Homem", checked: false, value: 1 }
    ];

    $rootScope.choicewoman = false;
    $rootScope.choiceman = false;
    $rootScope.age = 18;
    $rootScope.showme = 1;
    $rootScope.showmyage = 1;

    //Age Range Selector controllers.
    $rootScope.barProgressMin = 20;
    $rootScope.barProgressMax = 30;
    //Go to external URL
    $rootScope.goto = function (url) {
      window.open(url, '_system', 'location=no');
    };
    //Save Data on Device DB
    $rootScope.SaveLogin = function (e, p, i) {
      if (localStorageService.isSupported) {
        localStorageService.set('email', e);
        localStorageService.set('password', p);
        localStorageService.set('id', i);
      } else if (localStorageService.cookie.isSupported) {
        localStorageService.cookie.set('email', e);
        localStorageService.cookie.set('password', p);
        localStorageService.cookie.set('id', i);
      }
    };
    //Save Data on Device DB
    $rootScope.SaveLConfiguration = function (key, val) {
      if (localStorageService.isSupported) {
        localStorageService.set(key, val);
      } else if (localStorageService.cookie.isSupported) {
        localStorageService.cookie.set(key, val);
      }
    };
    //Get Data from Device DB
    $rootScope.GetConfiguration = function (key) {
      if (localStorageService.isSupported) {
        return localStorageService.get(key);
      } else if (localStorageService.cookie.isSupported) {
        return localStorageService.cookie.get(key);
      }
    };
    //Alerts and Load
    $rootScope.loadshow = function () {
      $ionicLoading.show({
        template: '<ion-spinner name="crescent"></ion-spinner>'
      });
    };
    $rootScope.loadhide = function () {
      $ionicLoading.hide();
    };
    $rootScope.alert = function (title, message) {
      $ionicPopup.alert({
        title: title,
        template: message
      });
    };
    //HTTP global function
    $rootScope.http_generator = function (url, parameter, done = null, undone = null, headers = { "Access-Control-Allow-Origin": "*" }) {
      
      $http.post(url, parameter, headers)
        .then(data => {
          console.log("data.status: " + data.status + " | url: " + url);
          console.log("data.data: " + data.data);
          if (done !== null) {
            done(data.data);
          }
          $rootScope.loadhide();
          return true;
        })
        .catch(error => {
          console.log("error.status: " + error.status + " | url: " + url);
          console.log("error.data: " + error.data);
          console.log(url);
          if (undone !== null) {
            undone();
          }
          $rootScope.loadhide();
          return false;
        });
    };
    //Other Functions
    $rootScope.gettoprof = function () {
      $rootScope.ProfList = [];
      x = 0;
      for (i in $rootScope.ImgList) {
        if ($rootScope.ImgList[i].number !== 0) {
          $rootScope.ProfList[x] = $rootScope.ImgList[i];
          x += 1;
        }
      }
      $ionicSlideBoxDelegate.update();
    };
    /* Get Configuration */
    $rootScope.gethttpconf = function () {
      user_id = $rootScope.GetConfiguration("id");

      $rootScope.http_generator($rootScope.conf_url,
        JSON.stringify({
        action: "get",
        id: user_id
        }), function (r) {
          response = JSON.stringify(r);
          response = JSON.parse(response);
          if (response.response !== "undefined" && response.response === "success") {
            //Configuration Input Setted Values.
            $rootScope.barProgressMin = response.rangemin;
            $rootScope.barProgressMax = response.rangemax;
            //Gender controllers
            $rootScope.gender = parseInt(response.gender); // Gender is 0 or 1.
            //Gender toggle creator and controller
            $rootScope.GenderList = [
              { text: "Mulher", checked: $rootScope.gender === 0 ? true : false, value: 0 },
              { text: "Homem", checked: $rootScope.gender === 1 ? true : false, value: 1 }
            ];

            $rootScope.choicewoman = response.choicewoman === "true";
            $rootScope.choiceman = response.choiceman === "true";

            $rootScope.age = parseInt(response.age);

            $rootScope.showme = response.showme === "true";

            $rootScope.showmyage = response.showmyage === "true";

            $rootScope.tel = response.tel;

            $rootScope.newmatches = $rootScope.GetConfiguration("newmatches");
            $rootScope.message = $rootScope.GetConfiguration("message");
            $rootScope.drinks = $rootScope.GetConfiguration("drinks");
            $rootScope.notifpush = $rootScope.GetConfiguration("notifpush");
            $rootScope.notifmail = $rootScope.GetConfiguration("notifmail");

            $rootScope.name = response.name;
            $rootScope.lastname = response.lastname;
            $rootScope.imageprofile = response.imageprofile;

            $rootScope.job = response.job;
            $rootScope.work = response.work;

            $rootScope.bio = response.bio;

            $rootScope.school = response.school;
          } else {
            console.log("get_conf_response: " + response);
          }
        }, function (error) {
          console.log("get_conf_error: " + error);
        });
    };
    /* /Get Configuration */
    /* Set Configuration */
    $rootScope.updhttpconf = function (key, value) {
      user_id = $rootScope.GetConfiguration("id");
      
      $rootScope.http_generator($rootScope.conf_url,
        JSON.stringify({
          action: "upd",
          id: user_id,
          key: key,
          value: value.toString()
        }), function (r) {
          response = JSON.stringify(r);
          response = JSON.parse(response);
          if (response.response === "undefined" || response.response !== "success") {
            $rootScope.alert("Oh...", "Não conseguimos atualizar seu perfil.");
          }
        }, function () {
          $rootScope.alert("Oh...", "Não conseguimos atualizar seu perfil.");
        });
    };

    $rootScope.attprofpic = function () {
      $rootScope.ImgList[0] = { number: 0, src: $rootScope.defprof, icon: 'icon ion-plus-circled photo-button assertive light-bg text-2x rounded' };
      
      $rootScope.http_generator($rootScope.img_url,
        JSON.stringify({
          action: "profget",
          id: user_id
        }), function (r) {
          response = JSON.stringify(r);
          response = JSON.parse(response);

          if (response.response !== "undefined" && response.response === "success") {
            if (response.image !== $rootScope.defprof) {
              $rootScope.ImgList[0].number = 1;
              $rootScope.ImgList[0].src = response.image;
              $rootScope.ImgList[0].icon = 'icon ion-close-circled photo-button assertive light-bg text-2x rounded';
              $rootScope.gettoprof();
            }
          }
        });
    };
    $rootScope.attpic = function () {
      $rootScope.ImgList[1] = { number: 0, src: $rootScope.defprof, icon: 'icon ion-plus-circled photo-button assertive light-bg text-2x rounded' };
      $rootScope.ImgList[2] = { number: 0, src: $rootScope.defprof, icon: 'icon ion-plus-circled photo-button assertive light-bg text-2x rounded' };
      $rootScope.ImgList[3] = { number: 0, src: $rootScope.defprof, icon: 'icon ion-plus-circled photo-button assertive light-bg text-2x rounded' };
      $rootScope.ImgList[4] = { number: 0, src: $rootScope.defprof, icon: 'icon ion-plus-circled photo-button assertive light-bg text-2x rounded' };
      $rootScope.ImgList[5] = { number: 0, src: $rootScope.defprof, icon: 'icon ion-plus-circled photo-button assertive light-bg text-2x rounded' };
      
      //Get profile photo, the first of the list
      $rootScope.http_generator($rootScope.img_url,
        JSON.stringify({
          action: "get",
          id: user_id
        }), function (r) {
          response = JSON.stringify(r);
          response = JSON.parse(response);

          if (response.response !== "undefined" && response.response === "success") {
            x = 1;
            for (var i in response.image) {
              if (response.image[x].id !== "0") {
                $rootScope.ImgList[x].number = response.image[x].id;
                $rootScope.ImgList[x].src = response.image[x].url;
                $rootScope.ImgList[x].icon = 'icon ion-close-circled photo-button assertive light-bg text-2x rounded';
                $rootScope.gettoprof();
                x += 1;
              } else { break; }
            }
          }
        });
    };

    $rootScope.updmatch = function (type, matched_id) {
      user_id = $rootScope.GetConfiguration("id");

      $rootScope.http_generator($rootScope.like_url,
        JSON.stringify({
          action: type,
          id: user_id,
          matched_id: matched_id
        }));
    };
    $rootScope.terms = function () {
      $ionicPopup.alert({
        title: 'Termos de Uso e Politicas.',
        template: 'Acesse o site http://bardapaquera.com.br e fique atualizado.',
        buttons: [
          { text: 'Voltar' }]
      });
    };
    /* /Set Configuration */
    
    /* Modals */
    $ionicModal.fromTemplateUrl('templates/modals/profile.html', {
      scope: $rootScope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $rootScope.profileModal = modal;
    });
    $ionicModal.fromTemplateUrl('templates/modals/profile_edit.html', {
      scope: $rootScope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $rootScope.editProfileModal = modal;
      });

    //Here take gethttpconf function on execute the modal
    $rootScope.closeProfileModal = function () {
      $rootScope.gethttpconf();
      $rootScope.attpic();
      $rootScope.attprofpic();
      $rootScope.gettoprof();
      $rootScope.profileModal.hide();
    };
    $rootScope.closeEditProfileModal = function () {
      $rootScope.gethttpconf();
      $rootScope.attpic();
      $rootScope.attprofpic();
      $rootScope.gettoprof();
      $rootScope.editProfileModal.hide();
    };
    $rootScope.openProfileModal = function () {
      $rootScope.gethttpconf();
      $rootScope.attpic();
      $rootScope.attprofpic();
      $rootScope.gettoprof();
      $rootScope.profileModal.show();
    };
    $rootScope.openEditProfileModal = function () {
      $rootScope.gethttpconf();
      $rootScope.attpic();
      $rootScope.attprofpic();
      $rootScope.gettoprof();
      $rootScope.editProfileModal.show();
    };
    /* /Modals */
    $rootScope.$state = $state;
  });
