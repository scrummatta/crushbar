// Source: https://www.thepolyglotdeveloper.com/2015/01/making-tinder-style-swipe-cards-ionic-framework/
angular.module('starter.directives', [])
  .directive('noScroll', function () {
    return {
      restrict: 'A',
      link: function ($scope, $element, $attr) {
        $element.on('touchmove', function (e) {
          e.preventDefault();
        });
      }
    };
  })
  .directive('anyphoto', function () {
    return {
      restrict: 'AE',
      scope: {
        number: '=',
        imgSrc: '='
      },
      template: '' +
        '<div class="photo-pickable" ng-click="pick()">' +
        '<img ng-src="{{imgSrc}}" class="w-full r-3x" alt="">' +
        '<i class="{{icon}}"></i>' +
        '</div>',
      controller: function ($scope, $rootScope, $http) {
        if ($scope.number === 0) {
          $scope.imgSrc = 'img/cam.png';
          $scope.icon = 'icon ion-plus-circled photo-button assertive light-bg text-2x rounded';
        } else {
          $scope.icon = 'icon ion-close-circled photo-button assertive light-bg text-2x rounded';
        }
        //Get from server and put here
        //$scope.icon = 'ion-plus-circled';
        //Get Galery image URL
        $scope.pick = function () {
          $rootScope.loadshow();
          //Get stored user ID
          user_id = $rootScope.GetConfiguration("id");
          if ($scope.number === 0) {
            document.addEventListener("deviceready", function () { 
            console.log("Any Photo");

            navigator.camera.getPicture(function (imageData) {
              //For get base64
              //base64img = "data:image/jpeg;base64," + imageData;

              //FileTransfer Object
              var ft = new FileTransfer();
              //File Upload Options
              filename = new Date();
              filename = filename.getHours() + "-" + (filename.getMinutes() + 1) + "-" + filename.getSeconds()+".jpg";
              let options = new FileUploadOptions();
              options.fileKey = "photo";
              options.fileName = filename;
              options.mimeType = "image/jpeg";
              options.chunkedMode = false;
              options.headers = { Connection: "close" };
              //Params
              var params = new Object();
              params.id = user_id;
              options.params = params;

              //Send image to server and after get from server and set on imgSrc
              ft.upload(imageData, $rootScope.upl_img_url,
                function (data) {
                  console.log("SEND_WORK");
                  response = JSON.parse(data.response);

                  console.log(response);
                  if (response.response !== "undefined" && response.response === "success") {

                    $scope.imgSrc = response.url;
                    $scope.number = response.id;
                    $scope.icon = 'icon ion-close-circled photo-button assertive light-bg text-2x rounded';
                  } else {
                    console.log("SEND_NOT_SUCCESS");
                  }

                  $rootScope.loadhide();
                },
                function (err) {
                  $rootScope.loadhide();

                  console.log("SEND_ERR");
                  console.log(err);
                },
                options);

            }, function (err) {
              // error
              $rootScope.loadhide();

              console.log("CAM_ERR");
              console.log(err);
            },
              {
                quality: 80,
                destinationType: navigator.camera.DestinationType.FILE_URI,
                sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: true,
                encodingType: navigator.camera.EncodingType.JPEG,
                targetWidth: 640,
                targetHeight: 640,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
              });
            }, false);
          } else {
            console.log("Delete Photo");

            let parameter = JSON.stringify({
              action: "del",
              id: user_id,
              image_id: $scope.number
            });

            $http.post($rootScope.img_url, parameter, $rootScope.headers)
              .then(data => {
                console.log(data.status);
                //console.log(data.data);
                //console.log(data.headers);

                response = JSON.stringify(data.data);
                response = JSON.parse(response);

                if (response.response !== "undefined" && response.response === "success") {
                  $scope.number = 0;
                  $scope.imgSrc = 'img/cam.png';
                  $scope.icon = 'icon ion-plus-circled photo-button assertive light-bg text-2x rounded';
                }
                $rootScope.loadhide();
              })
              .catch(error => {
                console.log(error.status);
                console.log(error.error);

                $rootScope.loadhide();
                //console.log(error.headers);
              });
          }
        };
      }
    };
  });
